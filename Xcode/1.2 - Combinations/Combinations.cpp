/*
 * File: Combinations.cpp
 * ----------------------
 * Name: Bohui Moon
 * Section: [TODO: enter section leader here]
 * This file is the starter project for the Combinations problem.
 * 
 * Provides methods dealing with calculating combinations of values. 
 * Uses a recursive solution through properties of Pascal's Triangle.
 * 
 * Functions:
 *      combination(int, int) - returns combination value according to given row & col of Pascal's Triangle
 *      getCombinations() - repeatedly asks the user for a row & col in Pascal's Triangle to print the combination value
 *      pascalsTriangle(int) - prints Pascal's Triangle according to the given int 
 */

#include <iostream>
#include <string>
#include "strlib.h"
#include "console.h"
using namespace std;

/* Function prototye */
string getLine(string message);
void getCombinations();
void pascalsTriangle(int numRow);

/**
 * Function: main
 * Usage: int main()
 * -------------------------------
 * Calls getCombinations() to calculate the combinations of given values
 *
 * returns 0 - system exit;
 */
int main() 
{
    pascalsTriangle(10);
    getCombinations();
    return 0;
}

/**
 * Function: combination
 * Usage: int combo = combination(row, col);
 * ------------------------------------------
 * Calculates the combination with the given values using recursion and Pascal's Triangle
 * 
 * Base Case: k==0 (left most value of a row) || n==k (right most value of a row)
 *		return 1;
 * Reduction:
 *		c(n,k) = c(n-1,k) + c(n-1, k-1)
 */
int combination(int n, int k)
{
	if (k==0 || (n==k))
		return 1;
	return combination(n-1, k-1) + combination(n-1, k);
}

/**
 * Function: getCombinations
 * Usage: getCombinations();
 * -----------------------------
 * Indefinitely gets the user's input for values of row & col of Pascal's Triangle
 * and displays the combination value
 */
void getCombinations()
{
	cout << "Type in the row and column of Pascal's Triangle" << endl;

	while (true)
	{
		int n = stringToInteger(getLine("n: "));
		int k = stringToInteger(getLine("k: "));

		cout << combination(n,k) << endl << endl;
	}
}

/**
 * Function: pascalsTriangle
 * Usage: pascalsTriangle(10);
 * ------------------------------------
 * Displays the given number of rows of Pascal's Triangle using combination(n,k)
 * Implements nested for-loops:
 *      - outter loop for the number of rows
 *      - inner loop runs according to the current row and calls combination(n,k)
 *
 * param: numRow - number of rows in Pascal's Triangle to display
 */
void pascalsTriangle(int numRow)
{
    for (int n=0; n<numRow; n++)
    {
        for (int i=0; i<numRow-n; i++)          //for formatting reasons
            cout << " ";
        for (int k=0; k<=n; k++)                //k does not exceed n in any row of Pascal's Triangle
            cout << combination(n,k) << " ";
        cout << endl;                           //new line for every row
    }
}