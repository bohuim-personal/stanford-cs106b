/**
 * File: Trailblazer.cpp
 * -----------------------------
 * Implementation of the graph algorithms that comprise the Trailblazer assignment.
 */

#include "Trailblazer.h"
#include "TrailblazerGraphics.h"
#include "TrailblazerTypes.h"
#include "TrailblazerPQueue.h"

/**
 * Function: shortestPath
 * ------------------------------
 * Finds the shortest path between the locations given by start and end in the
 * specified world using Dijkstra's algorithm. The cost of moving from one edge 
 * to the next is specified by the given cost function.	The resulting path 
 * is then returned as a Vector<Loc> containing the locations to visit 
 * in the order in which they would be visited.	If no path is found, 
 * this function should report an error.
 *
 * HashMap maps a location on the grid to the corresponding Node.
 * Priority Queue stores locations in priority.
 * Start with the start loc & initially set it to YELLOW.
 * Run while queue is not empty
 *      - Dequeue
 *      - set current to GREEN
 *      - Check around the location (except the current)
 *      - if location has not be checked before
 *          - color YELLOW, set Node's dist = currenst dist + costFn
 *          - enqueue loc as the value above
 *          - set parent as current
 *      - if its in already in the queue
 *          - set Node's dist = currenst dist + costFn
 *          - enqueue loc as the value above
 *          - set parent as current
 *
 * Start with End node and trace back to start
 * adding to Vector<Loc> and return the path.
 * 
 * @param start - location of shortest path to find
 * @param end - location of shortest path to find
 * @param world - the terrain to find the path on
 * @param costFunction() - calculates the cost between two nodes
 * @return path - Vector of locations of shortest path
 * 
 * @exception no path was found if path only has one location
 */
Vector<Loc> shortestPath(Loc start, Loc end, Grid<double>& world,
             double costFn(Loc from, Loc to, Grid<double>& world)) 
{	
    TrailblazerPQueue<Loc> queue;
    HashMap<Loc, Node> graph;
    for (int r=0; r<world.numRows(); r++)
        for (int c=0; c<world.numCols(); c++)
            graph.put(makeLoc(r,c), makeNode(r,c));

    queue.enqueue(start, 0);
    colorCell(world, start, YELLOW);
    graph[start].color = YELLOW;
    
    while (!queue.isEmpty())
    {
        Loc current = queue.dequeueMin();
        colorCell(world, current, GREEN);
        double dist = graph[current].distance;
        if (current == end)
            break;
        
        for (int r=current.row-1; r<=current.row+1; r++)
            for (int c=current.col-1; c<=current.col+1; c++)
                if (0 <= r && r < world.numRows() && 0 <= c && c < world.numCols())
                {
                    Loc v = makeLoc(r,c);
                    double newDist = dist + costFn(current, v, world);
                    if (graph[v].color == GRAY)
                    {
                        colorCell(world, v, YELLOW);
                        graph[v].color = YELLOW;
                        graph[v].distance = newDist;
                        graph[v].parent = &graph[current];
                        queue.enqueue(v, newDist);
                    }
                    else if (v!=current && graph[v].color == YELLOW && graph[v].distance >= newDist)
                    {
                        graph[v].distance = newDist;
                        graph[v].parent = &graph[current];
                        queue.decreaseKey(v, newDist);
                    }
                }
    }
	
    Vector<Loc> path;
    Node* currentNode = &graph[end];
    while (currentNode->parent != NULL)
    {
        path.add(currentNode->location);
        currentNode = currentNode->parent;
    }
    if (path.size() == 1)
        error("No path found");
    return path;
}

/**
 * Function: shortestPath
 * ----------------------------
 * Finds the shortest path between the locations given by start and end in the
 * sepcified world using A* algorithm. The cost of movign from one edge to the 
 * net is specified by the given cost funciton. The resulting path is then returned
 * as a Vector<Loc> containing the locations to visit in order in which they 
 * would be visited. If no path is found, this function should report an error.
 *
 * HashMap maps a location on the grid to the corresponding Node.
 * Priority Queue stores locations in priority while Set<Loc> 
 * keeps track of which locations are in the queue (contains() not supported in Queue).
 * Start with the start loc & initially set it to YELLOW.
 * Run while queue is not empty
 *      - Dequeue & remove from set
 *      - set current to GREEN
 *      - Check around the location (except the current)
 *      - if location has not be checked before
 *          - color YELLOW, set Node's dist = currenst dist + costFn
 *          - enqueue loc as the value above + heuristic
 *          - set parent as current
 *      - if its in already in the queue
 *          - set Node's dist = currenst dist + costFn
 *          - enqueue loc as the value above + heuristic
 *          - set parent as current
 *
 * Start with End node and trace back to start
 * adding to Vector<Loc> and return the path.
 * 
 * @param start - location of shortest path to find
 * @param end - location of shortest path to find
 * @param world - terrain of the path to find
 * @param costFunction() - calculates the cost between two nodes
 * @param heuristic() - given start & end gives guess of distance of the path
 * @return path - Vector of locations of shortest path
 * 
 * @exception no path was found if path only has one location
 */
Vector<Loc> shortestPath(Loc start, Loc end, Grid<double>& world,
             double costFunction(Loc one, Loc two, Grid<double>& world),
             double heuristic(Loc start, Loc end, Grid<double>& world))
{
    HashMap<Loc, Node> graph;
    for (int r=0; r<world.numRows(); r++)
        for (int c=0; c<world.numCols(); c++)
            graph.put(makeLoc(r,c), makeNode(r,c));
    
    TrailblazerPQueue<Loc> queue;
    Set<Loc> inQueue;
    queue.enqueue(start, heuristic(start, end, world));
    inQueue.add(start);
    
    colorCell(world, start, YELLOW);
    graph[start].color = YELLOW;
    
    while (!queue.isEmpty())
    {
        Loc current = queue.dequeueMin();
        inQueue.remove(current);
        colorCell(world, current, GREEN);
        double dist = graph[current].distance;
        if (current == end)
            break;
        
        for (int r=current.row-1; r<=current.row+1; r++)
            for (int c=current.col-1; c<=current.col+1; c++)
                if (0 <= r && r < world.numRows() && 0 <= c && c < world.numCols())
                {
                    Loc v = makeLoc(r,c);
                    double newDist = dist + costFunction(current, v, world);
                    if (graph[v].color == GRAY)
                    {
                        colorCell(world, v, YELLOW);
                        graph[v].color = YELLOW;
                        graph[v].distance = newDist;
                        graph[v].parent = &graph[current];
                        queue.enqueue(v, newDist + heuristic(v, end, world));
                        inQueue.add(v);
                    }
                    else if (v!=current && inQueue.contains(v) && 
                             graph[v].color == YELLOW && graph[v].distance >= newDist)
                    {
                        graph[v].distance = newDist;
                        graph[v].parent = &graph[current];
                        queue.decreaseKey(v, newDist + heuristic(v, end, world));
                    }
                }
    }
	
    Vector<Loc> path;
    Node* currentNode = &graph[end];
    while (currentNode->parent != NULL)
    {
        path.add(currentNode->location);
        currentNode = currentNode->parent;
    }
    if (path.size() == 1)
        error("No path found");
    return path;
}

/**
 * Function: createMaze
 * -------------------------------
 * Creates a maze of the specified dimensions using a randomized version of
 * Kruskal's algorithm, then returns a set of all of the edges in the maze.
 *
 * Each location in the grid of the given dimension to a Set<Loc> that 
 * initially only contains that location. 
 * All possible edges of the grid are added into a Set<Edge> allEdges.
 * To randomly assign edge weights to each edge, edges are simply
 * enqueued into a priority queue with a random doublepriority value [0,1].
 * The number of clusters starts as many locations in the grid and
 * is decremented every time clusters are merged.
 * While the number of clusters is bigger than 1
 *      - dequeue an edge. 
 *      - if the clusters of start & end locations are not even
 *          - add the edge to result
 *          - create a new cluster containing all locs from start & end clusters
 *          - set the new cluster as the cluster at the locations in start & end
 * return the result edges
 * 
 * @param numRows - number of rows in the maze
 * @param numCols - number of columns in the maze
 */
Set<Edge> createMaze(int numRows, int numCols) 
{
    Map<Loc, Set<Loc> > map;
    Set<Edge> allEdges;
    for (int r=0; r<numRows; r++)
        for (int c=0; c<numCols; c++)
        {
            Loc current = makeLoc(r,c);
            Set<Loc> temp;
            temp.add(current);
            map.put(current, temp);
            
            if (r+1 < numRows)
                allEdges.add(makeEdge(current, makeLoc(r+1,c)));
            if (c+1 < numCols)
                allEdges.add(makeEdge(current, makeLoc(r,c+1)));
        }
        
    TrailblazerPQueue<Edge> queue;
    foreach (Edge e in allEdges)
        queue.enqueue(e, randomReal(0,1));
    
    Set<Edge> result;
    int numClusters = numRows * numCols;
    while (numClusters > 1)
    {
        Edge e = queue.dequeueMin();
        if (map[e.start] != map[e.end])
        {
            result.add(e);
            numClusters--;
            
            Set<Loc> combo = map[e.start] + map[e.end];
            foreach (Loc loc in map[e.start])
                map[loc] = combo;
            foreach (Loc loc in map[e.end])
                map[loc] = combo;
        }
    }
    return result;
}

/** 
 * Function: makeNode
 * -------------------------
 * Given the (row, col) to a location,
 * makes and returns a Node representing that 
 * location on the grid.
 * 
 * @return result - Node representation of given location
 */
Node makeNode(int r, int c)
{
    Node n(r,c);
    return n;
}






