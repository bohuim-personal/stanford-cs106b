/**
 * File: Trailblazer.h
 * -----------------------------
 * Exports functions that use Dijkstra's algorithm, A* search, and Kruskal's
 * algorithm as specified in the assignment handout.
 */

#ifndef Trailblazer_Included
#define Trailblazer_Included

#include "TrailblazerTypes.h"
#include "set.h"
#include "grid.h"
#include "random.h"
#include "queue.h"
#include "stack.h"
#include "map.h"
#include "hashmap.h"
#include <string>
using namespace std;

/**
 * @struct: Node
 * ---------------------------
 * Node is a representation of a graph Node
 * for Dijkstra & A* algorithms
 * Keeps track of the color, distance, parent and Loc
 */
struct Node 
{
    /* Default Constructor */
    Node()
    {
        color = GRAY;
        distance = 0;
        parent = NULL;
    }
    
    Node(int r, int c)
    {
        color = GRAY;
        distance = 0;
        parent = NULL;
        location = makeLoc(r,c);
    }
    
    int color;
    double distance;
    Loc location;
    Node* parent;
};

struct MazeNode : Loc
{
    MazeNode() {}
    
    MazeNode(int r, int c)
    {
        row = r;
        col = c;
        cluster.add(*this);
    }
    
    Set<Loc> cluster; 
};

/**
 * Function: shortestPath (Dijkstra)
 * -----------------------------
 * Finds the shortest path between the locations given by start and end in the
 * specified world.	The cost of moving from one edge to the next is specified
 * by the given cost function.	The resulting path is then returned as a
 * Vector<Loc> containing the locations to visit in the order in which they
 * would be visited. If no path is found, this function should report an
 * error.
 */
//Vector<Loc> shortestPath(Loc start, Loc end, Grid<double>& world,
//             double costFn(Loc from, Loc to, Grid<double>& world));

/**
 * Function: shortestPath (A*)
 * ----------------------------
 * Finds the shortest path between the locations given by start and end in the
 * sepcified world using A* algorithm. The cost of movign from one edge to the 
 * net is specified by the given cost funciton. The resulting path is then returned
 * as a Vector<Loc> containing the locations to visit in order in which they 
 * would be visited. If no path is found, this function should report an error.
 */
Vector<Loc> 
shortestPath(Loc start, Loc end, Grid<double>& world,
            double costFunction(Loc one, Loc two, Grid<double>& world),
            double heuristic(Loc start, Loc end, Grid<double>& world));

/**
 * Function: createMaze
 * --------------------------
 * Creates a maze of the specified dimensions using a randomized version of
 * Kruskal's algorithm, then returns a set of all of the edges in the maze.
 *
 * As specified in the assignment handout, the edges you should return here
 * represent the connections between locations in the graph that are passable.
 * Our provided starter code will then use these edges to build up a Grid
 * representation of the maze.
 */
Set<Edge> createMaze(int numRows, int numCols);

/**
 * Function: makeNode
 * -------------------------
 * Given (row, col) constructs a Node
 * with the given the loc
 */
Node makeNode(int r, int c);

/* for debugging purposes */
string getLine(string mssg);

#endif
