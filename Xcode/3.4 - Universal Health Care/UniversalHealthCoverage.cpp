/**
 * File: UniversalHealthCoverage.cpp
 * ----------------------
 * Name: Bohui (Dennis) Moon
 * Section: Steve Cooper
 * This file is the starter project for the UniversalHealthCoverage problem on Assignment #3.
 *
 * Provides one function for checking whether health coverage to all cities are possible
 * Extension: given the information on cities and their populations, new function returns max number of people who can get coverage
 * 
 * Functions:
 *      - canOfferUniversalCoverages(cities, hospitals, hopitalsLeft, workingComboOfHospitals): given the parameters, returns whether the hospitals can cover all the ciites in the nation
 *      - maxCoverage(populations, hospitals, hospitalsLeft, workingComboOfHospitals): given the parameters, return the max number of people who can receive coverage
 *
 * Operators:
 *      "<" - defined for Set<string> so that Set< Set<string> > can sort its elements
 */
 
#include <iostream>
#include <string>
#include "foreach.h"
#include "set.h"
#include "vector.h"
#include "map.h"
#include "console.h"
using namespace std;

/* Function Prototypes */
    bool canOfferUniversalCoverage(Set<string>& cities, Vector< Set<string> >& locations, int numHospitals, Vector< Set<string> >& result);
    Set<string> uncoveredCities(Set<string> cities, Set<string> location);
    int maxCoverage(Map<string, int> populations, Vector< Set<string> >& locations, int numHospitals, Set< Set<string> >& result);
    bool operator < (const Set<string>& a, const Set<string>& b);

/**
 * Function: main
 * Usage: int main()
 * ----------------------------------
 * No input from user
 */
int main() 
{
    return 0;
}


/**
 * Function: canOfferUniversalCoverage(Set<string>& cities, Vector< Set<string> >& locations, int numHospitals, Vector< Set<string> >& result);
 * Usage: if (canOfferUniversalCoverage(cities, locations, 4, result)
 * ==================================================================
 * Given a set of cities, a list of what cities various hospitals can
 * cover, and a number of hospitals, returns whether or not it's
 * possible to provide coverage to all cities with the given number of
 * hospitals.  If so, one specific way to do this is handed back in the
 * result parameter.
 *
 * Base Case: no hospitals can be built
 *      - if there are no cities, all cities can be covered
 *
 * Reduction:
 *      - assume that hospital works & add hopital to result
 *      - create a copy of the list of hospitals
 *      - cover the cities the hospital provides coverage for by subtracting from cities left
 *      - remove the used hospital from the copy list
 *      - if any of the combinations of hospitals work, return true;
 *      - getting pass that point means, that combo didnt work, so remove the hospital from result
 *      - return false outside the for loop
 */
bool canOfferUniversalCoverage(Set<string>& cities, Vector< Set<string> >& locations, int numHospitals, Vector< Set<string> >& result)
{
    if (numHospitals == 0)
        return cities.size() == 0;
    
    for (int i=0; i<locations.size(); i++) //use for loop because vector does not provide remove(obj) 
    {
        result.add(locations[i]);
        
        Vector< Set<string> > copy = locations;             //create a copy to pass a fresh copy for ever iteration
        Set<string> uncoveredCities = cities - copy.get(i); //subtract cities that are covered
        copy.remove(i);                                     //remove that hospital from the list
        
        if (canOfferUniversalCoverage(uncoveredCities, copy, numHospitals-1, result)) //if any of these combinations works
            return true;                                                              //return true;
            
        result.remove(result.size()-1); //remove the last added hospital if it does not work
    }
    return false; //false if none of the combinations worked
}

/**
 * Function: maxCoverage
 * Usage: maxCoverage(populations, hospitalLocs, numHospitals, result)
 * ---------------------------------------------------------------------------
 * Returns the maximum number that can receive health coverage given the number of hospitals that can be built
 * Essentially the same as canOfferUniversalCoverage(), but with populations
 *
 * Base Case: no hospitals can be built
 *      - 0 people receive coverage
 * Reduction:
 *      - look at the coverage for each hospital location
 *      - create copies of populations & locations to pass fresh copy on every recursion
 *      - look at the cities covered by a hospital & add up the populations of covered cities
 *          - remove city from Map to prevent recounting a population
 *          - remove the hospital which has already been processed
 *      - get the total coverage of a certain combo of hospitals by calling maxCoverage
 *          - if its bigger than the last combo, add the hospital to result & set new max
 *      - return max
 */
int maxCoverage(Map<string, int> populations, Vector< Set<string> >& locations, int numHospitals, Set< Set<string> >& result)
{
    if (numHospitals == 0)
        return 0;
    
    int max = 0;
    for (int i=0; i<locations.size(); i++)
    {
        Map<string, int> copyPop = populations;
        Vector< Set<string> > copyLoc = locations; 
        
        int currentCoverage = 0;
        foreach (string city in locations[i])
        {
            currentCoverage += populations[city];
            copyPop.remove(city);
        }
        copyLoc.remove(i);
        
        int sum = currentCoverage + maxCoverage(copyPop, copyLoc, numHospitals-1, result);
        if (sum > max)
        {
            result.add(locations[i]);
            max = sum;
        }
    }
    return max;
}

/**
 * Operator: <
 * Usage: (Set<string> a) < (Set<string> b)
 * --------------------------------------------------
 * Defined so that a Set can contain Set<string> as its elements
 * 
 * Order of sets only depend on their sizes
 */
bool operator < (const Set<string>& a, const Set<string>& b)
{
    if (a.size() < b.size())
        return true;
    return false;
}

