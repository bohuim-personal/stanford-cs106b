/*
 * File: NumericConversions.cpp
 * ---------------------------
 * Name: Bohui Moon
 * Section: [TODO: enter section leader here]
 * 
 * Re-implements conversion functions that convert int to string or vise versa
 * Both conversion functions implement a recursive solution
 * 
 * Function:
 *      intToString(int) - returns the string representation of the given int value 
 *      stringToInt(string) - returns the string representation of the given string 
 *
 * Works Cited:
 *      cpluplus.com reference on random number generator ("http://www.cplusplus.com/reference/cstdlib/rand/")
 */

#include <iostream>
#include <string>
#include "random.h"
#include "strlib.h"
#include "console.h"
using namespace std;

/* Function prototypes */
void pickConversion();
string getLine(string message = "");
string intToString(int n);
int stringToInt(string str);

string intToHexString(int n);
int hexStringToInt(string hex);

int intToBinary(int n);
string intToBinaryString(int n);
int binaryToInt(int n);

int numeralToInt(string numeral);
string intToNumeral(int n);

/* Code */

/**
 * Function: main
 * Usage: int main()
 * ------------------------
 * Tests the conversion functions by calling tester methods
 */
int main() 
{
    pickConversion();
    return 0;
}

/**
 * Function: pickConversion
 * Usage: pickConversion();
 * ---------------------------------
 * Allows user to chooose conversion function to use from console
 * does not include int -> string because console input is string and needs to be convertedan int parameter 
 */
void pickConversion()
{
    cout << "1. String -> Integer" << endl 
    << "2. Integer -> Hexadecimal" << endl
    << "3. Hexadecimal -> Integer" << endl
    << "4. Integer -> Binary" << endl
    << "5. Binary -> Integer" << endl
    << "6. Integer -> Roman Numeral" << endl
    << "7. Roman Numeral -> Integer" << endl;
    
    while (true)
    {
        string choice = getLine("Choose a number: ");
        
        string input = getLine("Input: ");
        if (choice == "1") cout << stringToInt(input);
        else if (choice == "2") cout << intToHexString(stringToInt(input));
        else if (choice == "3") cout << hexStringToInt(input);
        else if (choice == "4") cout << intToBinaryString(stringToInt(input));
        else if (choice == "5") cout << binaryToInt(stringToInt(input));
        else if (choice == "6") cout << intToNumeral(stringToInt(input));
        else if (choice == "7") cout << numeralToInt(input);
        cout << endl << endl;
    }
}

/**
 * Function: intToString
 * Usage: string str = intToString(10);
 * --------------------------------------
 * Returns the string representation of the given int using recursion
 *
 * Special Case: n is negative
 *      return "-" + string representation of abs(n) or -n
 *
 * Base Case: n is 0
 *      return "0"
 *
 * Base Case: n is single digit
 *      return string representation of the number
 *
 * Reduction:
 *      separate the ones digit with n%10, and the rest with n/10
 *      return string representation of n/10 appended by directly converted ones digit
 *
 * Param: n - integer to convert to string form
 * Return: string form of given int
 */
string intToString(int n)
{
    if (n < 0)
        return "-" + intToString(-n);
    if (n == 0)
        return "0";
        
    string onesDigitString = string() + char(n%10 + '0');
    if (n < 10)
        return onesDigitString;
        
    return intToString(n/10) + onesDigitString;
}

/**
 * Function: stringToInt
 * Usage: int num = stringToInt("10");
 * --------------------------------------
 * Returns the int value of the given string using recursion
 *
 * Special Case: n is negative
 *      return -1 * stringToInt(all but first character)
 *
 * Base Case: n is single digit
 *      return the compare value of string to "0"
 *
 * Reduction:
 *      convert last digit into int by comparing string to "0"
 *      return stringToInt(all but last digit)*10 + last digit
 *
 * Param: numStr - string to convert into int
 * Return: int conversion of given string
 */
int stringToInt(string numStr)
{
    int length = numStr.length(); //for easier reading

    if (numStr.substr(0,1) == "-")
        return -1 * stringToInt(numStr.substr(1, length-1));
        
    int lastDigit = numStr.substr(length-1,1).compare("0");
    if (length == 1)
        return lastDigit;
        
    return stringToInt(numStr.substr(0,length-1))*10 + lastDigit;
}

/**
 * Function: intToHexString
 * Usage: string hex = intToHexString(100);
 * --------------------------------------
 * Returns the given int in hexadecimal in string format
 *
 * Special Case: n is negative
 *      return "-" + intToHexString(-n)
 *
 * Base Case: n is [0,9]
 *      return string representation of n
 *
 * Base Case: n is [10,15]
 *      return "A" ~ "F" where "A" corresponds to 10 and so on
 *
 * Reduction:
 *      reduce n into two parts: n/16 & n%16 to convert into base 16
 *      return the string representation of both parts combined
 *
 * Param: n - integer to convert into hexadecimal
 * Return: hexadecimal (string format) of given int
 */
string intToHexString(int n)
{
    if (n < 0)
        return "-" + intToHexString(-n);
    if (0<=n && n<=9)
        return intToString(n);
    if (10<=n && n<=15)
        switch(n)
        {
            case 10: return "A";
            case 11: return "B";
            case 12: return "C";
            case 13: return "D";
            case 14: return "E";
            case 15: return "F";
        }
    
    return intToHexString(n/16) + intToHexString(n%16);
}

/**
 * Function: hexStringToInt
 * Usage: int integer = hexStringToInt("A5F1");
 * -----------------------------------------------------
 * Returns integer equivalent of the given string representation of a hex number
 *
 * Special Case: n is negative
 *      return -1 * hexStringToInt(all but first character)
 *
 * Base Case: hexString is of length 1
 *      compare string with "0"
 *          - return comparison value if comparison is [0,9]
 *          - return comparison-7 if comparison is [17,22] because "A".compare("0") == 17
 *
 * Reduction:
 *      reduce n into two parts: n/16 & n%16 to convert into base 16
 *      return the string representation of both parts combined
 *
 * Param: hex - hexadecimal in string format to convert into int
 * Return: int form of given hexadecimal
 */
int hexStringToInt(string hex)
{
    int length = hex.length();              //for easier reading
    
    if (hex.substr(0,1) == "-")
        return -1 * hexStringToInt(hex.substr(1, length-1));
    
    if (length == 1)
    {
        int lastDigit = hex.substr(length-1, 1).compare("0");
        if (0<=lastDigit && lastDigit<=9)
            return lastDigit;
        if (lastDigit >= 17)                //hex contains letters A~F
            return lastDigit-7;
    }    
    
    return hexStringToInt(hex.substr(0, length-1))*16 + hexStringToInt(hex.substr(length-1, 1));
}

/**
 * Function: intToBinary
 * Usage: int binary = intToBinary(1023);
 * --------------------------------------
 * Returns the given integer in binary wrapped in int format
 *
 * Special Case: n is negative
 *      return -1 * intToBinary(-n)
 *
 * Base Case: n < 2
 *      return n
 *
 * Reduction:
 *      reduce n into two parts: n/2 & n%2 to convert into base 2
 *      convert each part into binary, multiply n/2 by 10
 *
 * Precondition: n < 1024, due to size problems with int format
 * Param: int value to convert into binary
 * Return: binary conversion in int format
 */
int intToBinary(int n)
{
    if (n < 0)
        return -1 * intToBinary(-n);
    if (n < 2)
        return n;
    return intToBinary(n/2)*10 + intToBinary(n%2);
}

/**
 * Function: intToBinaryString
 * Usage: string binary = intToBinaryString(2048);
 * --------------------------------------
 * Returns the given integer in binary wrapped in string format
 * Similar to intToBinary() but can handle input above 1023
 *
 * Special Case: n is negative
 *      return "-" + intToBinaryString(-n)
 *
 * Base Case: n < 2
 *      return string representation of n
 *
 * Reduction:
 *      reduce n into two parts: n/2 & n%2 to convert into base 2
 *      return each part int binary string format combined
 *
 * Param: int value to convert into binary
 * Return: binary conversion in string format
 */
string intToBinaryString(int n)
{
    if (n < 0)
        return "-" + intToBinaryString(-n);
    if (n < 2)
        return intToString(n);
    return intToBinaryString(n/2) + intToBinaryString(n%2);
}

/**
 * Function: binaryToInt
 * Usage: int n = binaryToInt(110101110);
 * --------------------------------------
 * Returns binary equivalent of given integer
 *
 * Special Case: n is negative
 *      return -1 * binaryToInt(-n)
 *
 * Base Case: n < 10 (2 in binary format)
 *      return n
 *
 * Reduction:
 *      reduce n into two parts: n/10 & n%10 to convert into int (10 here is 2 in binary)
 *      return binaryToInt(n/10)*2 + binaryToInt(n%10) 
 *
 * Param: binary value (int format) to convert into int
 * Return: int value conversion
 */
int binaryToInt(int n)
{
    if (n < 0)
        return -1 * binaryToInt(-n);
    if (n < 10) //2 in binary
        return n;
    return binaryToInt(n/10)*2 + binaryToInt(n%10); 
}

/**
 * Function: numeralToInt
 * Usage: int n = numeralToInt("XVI");
 * --------------------------------------
 * Returns int value of given Roman Numeral
 *
 * Precondition: 
 *      - numeral is not empty
 *      - numeral is a valid Roman Numeral; 
 *              function does not check for invalid numerals like "IIII"
 *
 * Base Case: one character
 *      retrun integer value according to compare value to "A"
 *
 * Reduction:
 *      reduce n into two parts: first letter & everything else
 *      if first >= else (like "VII"), add the base values
 *      otherwise (like "IV"), subtract
 *
 * Param: numeral - Roman Numeral in string form
 * Return: int value conversion
 */
int numeralToInt(string numeral)
{
    int length = numeral.length();

    if (length == 1)
        switch (toUpperCase(numeral).compare("A"))
        {
            case 8  : return 1;     //I
            case 21 : return 5;     //V
            case 23 : return 10;    //X
            case 11 : return 50;    //L
            case 2  : return 100;   //C
            case 3  : return 500;   //D
            case 12 : return 1000;  //M
        }
    
    int before = numeralToInt(numeral.substr(0,1));
    int after  = numeralToInt(numeral.substr(1,length-1));
    
    if (before >= after)
        return before + after;
    return after - before;
}

/**
 * Function: intToNumeral
 * Usage: string numeral = intToNumeral(67);
 * --------------------------------------
 * Returns Roman Numeral value of given integer
 *
 * Base Case: n is zero
 *      return "" because zero does not exist in Roman Numerals
 *
 * Reduction:
 *      Given an integer, find the highest Roman Numeral below it & the numeral's integer value
 *      Add to the found numeral: intToNumeral(n - numeral value)
 *
 * Param: n - integer to convert into Roman Numerals
 * Return: Roman Numeral form of given int
 */
string intToNumeral(int n)
{
    if (n==0)
        return "";
    
    //find highest possible numeral & its int val
    //below are all Roman Numerals & special subtraction numerals such as "IV"
    string highestNumeral = "";
    int highestVal = 0;
    if      (n >= 1000) { highestNumeral = "M";  highestVal = 1000; }
    else if (n >= 900)  { highestNumeral = "CM"; highestVal = 900;  }
    else if (n >= 100)  { highestNumeral = "C";  highestVal = 100;  }
    else if (n >= 90)   { highestNumeral = "XC"; highestVal = 90;   }
    else if (n >= 50)   { highestNumeral = "L";  highestVal = 50;   }
    else if (n >= 40)   { highestNumeral = "XL"; highestVal = 40;   }
    else if (n >= 10)   { highestNumeral = "X";  highestVal = 10;   }
    else if (n >= 9)    { highestNumeral = "IX"; highestVal = 9;    }
    else if (n >= 5)    { highestNumeral = "V";  highestVal = 5;    }
    else if (n >= 4)    { highestNumeral = "IV"; highestVal = 4;    }
    else if (n >= 1)    { highestNumeral = "I";  highestVal = 1;    }
    
    return highestNumeral + intToNumeral(n-highestVal);
}