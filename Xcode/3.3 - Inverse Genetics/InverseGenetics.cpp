/**
 * File: InverseGenetics.cpp
 * --------------------------
 * Name: Bohui (Dennis) Moon
 * Section: Steve Cooper
 * This file is the starter project for the Inverse Genetics problem on Assignment #3.
 *
 * Provides the functions to process all possible RNA codon sequences for a certain protein
 *
 * Functions:
 *      - listAllRNAStransFor(protein, codonMapping): wrapper function calls RNAHelper()
 *      - RNAHelper(protein, soFar, codonMapping): given a protein, the sequence so far, and a mapping of codons, prints out the sequence of possible codons
 */

#include <iostream>
#include <string>
#include <fstream>
#include "foreach.h"
#include "set.h"
#include "map.h"
//#include "console.h"
using namespace std;

/* Function Prototypes */
    string getLine(string mssg);
    void listAllRNAStrandsFor(string protein, Map<char, Set<string> >& codons);
    Map<char, Set<string> > loadCodonMap();
    void RNAHelper(string protein, string soFar, Map<char, Set<string> >& codons);

/** 
 * Function: main
 * Usage: int main()
 * -------------------------------------
 * Loads the protein to possible codons mapping to a Map using loadCodonMap();
 * Indefinitely asks user for a protein input & prints out the possible codons using listAllRNAStandsFor();
 */
int main() 
{
    Map<char, Set<string> > codons = loadCodonMap(); //Load the codon map
    
    while (true)
    {
        listAllRNAStrandsFor(getLine("Input protein (amino acids): "), codons);
        cout << endl;
    }
        
    return 0;
}

/** 
 * Function: listAllRNAStrandsFor(string protein, Map<char, Set<string> >& codons);
 * Usage: listAllRNAStrandsFor("PARTY", codons);
 * ==================================================================
 * Given a protein and a map from amino acid codes to the codons for
 * that code, lists all possible RNA strands that could generate that protein
 *
 * Calls RNAHelp because an extra string parameter is required
 */
void listAllRNAStrandsFor(string protein, Map<char, Set<string> >& codons)
{
    RNAHelper(protein, "", codons);
}

/**
 * Function: RNAHelper
 * Usage: RNAHelper(protein, "", codons);
 * -------------------------------------------
 * Given a protein (string of amino acids), prints out all possible codon sequences for that protein
 *
 * Base Case: no protein
 *      - print out soFar (whatever it is)
 * Reduction:
 *      - for each possible codons that produce the first amino acid
 *          - attach it to soFar and pass it onto RNAHelper with the amino acid removed from the protein
 */
void RNAHelper(string protein, string soFar, Map<char, Set<string> >& codons)
{
    if (protein.length() == 0)
        cout << soFar << endl;
    else
        foreach (string codon in codons[protein[0]])
        {
            RNAHelper(protein.substr(1), soFar+codon, codons);
        }
}

/** 
 * Function: loadCodonMap();
 * Usage: Map<char, Lexicon> codonMap = loadCodonMap();
 * ==================================================================
 * Loads the codon mapping table from a file.
 */
Map<char, Set<string> > loadCodonMap() 
{
    ifstream input("codons.txt");
    Map<char, Set<string> > result;
    
    // The current codon / protein combination.
    string codon;
    char protein;
    
    // Continuously pull data from the file until all data has been read.
    while (input >> codon >> protein) {
        result[protein] += codon;
    }
    
    return result;
}