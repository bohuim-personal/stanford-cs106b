/**
 * File: Sierpinski.cpp
 * --------------------------
 * Name: Bohui (Dennis) Moon
 * Section: Steve Cooper
 * This file is the starter project for the Sierpinski problem
 * on Assignment #3.
 *
 * Implements several funtions necessary to draw a Sierpinski Triangle
 * HEIGHT_FACTOR is a constant sqrt(3)/2 gives the height of a equilateral triangle when multiplied by length of a side
 * 
 * Functions:
 *      polarLine(x,y,r,t) - uses drawPolarLine of GWindow to draw a line so that the origin (0,0) is the left bottom of the screen
 *      drawTriangle(x,y,len) - draws a triangle so that the given (x,y) is the top vertex of the triangle
 *      sierpinskiTriangle(order,length) - wrapper function that either calls sierpinskiRecursion() or SierpinskiIteration()
 *      sierpinskiRecursion(order, length, x,y) - recursively draws Sierpinski's Triangle of given order with the topmost vertex at (x,y)
 *      sierpinskiIteratino(order, length) - iterative draws Sierpinski's Triangle of given order
 *
 * Overloaded Operators:
 *      "<" - the less than operator is defined for GPoint because Set<GPoint> requires comparison of GPoints to keep element in order 
 *
 * Citation:
 *      Operator Overloading (http://stackoverflow.com/questions/4421706/operator-overloading);
 */

#include <iostream>
#include <string>
#include "strlib.h"
#include "gwindow.h"
#include "foreach.h"
#include "map.h"
#include "set.h"
#include <cmath>
using namespace std;

/* Global Variables */
    GWindow* window;
    const double HEIGHT_FACTOR = sqrt(3)/2;
    bool useIteration;

/* Function Prototypes*/
    string getLine(string mssg);
    GPoint drawPolarLine(double x0, double y0, double r, double theta);
    GPoint polarLine(double x, double y, double length, double angle);
    void drawTriangle(double x, double y, double length);
    void sierpinskiTriangle(int order, int length);
    void sierpinskiRecursion(int order, int length, double x, double y);
    void sierpinskiIteration(int order, int length);
    
    bool operator < (const GPoint& a, const GPoint& b);

/* Code */

/**
 * Function: main
 * Usage: int main()
 * ----------------------------------------------
 * Attaches a new GWindow of dimension 600x600 to the global GWindow pointer
 * Indefinitely asks user for order & length to draw sierpinski triangle
 */
int main() 
{
    window = new GWindow(600,600);
    while (true)
    {
        window->clear();
        
        //sets whether to use iteration or recursion 
        useIteration = (getLine("Use Iteration instead of Recursion? ")=="yes" ? true : false); 
        
        //draw Sierpinski's Triangle according to given order & length
        sierpinskiTriangle(stringToInteger(getLine("Input order: ")), stringToInteger(getLine("Input length[100-600]: ")));  
               
        cout << endl;
        getLine("Enter to continue"); //waits before clearing GWindow
    }
    return 0;
}

/**
 * Function: polarLine
 * Usage: polarLine(x,y, length, 60);
 * -------------------------------------------------
 * Draws a line on GWindow using drawPolarLine(), 
 * but draws the line so that the origin (0,0) 
 */
GPoint polarLine(double x, double y, double length, double angle)
{
    return window->drawPolarLine(x, window->getHeight()-y, length, angle);
}

/**
 * Function: drawTriangle
 * Usage: drawTriangle(x,y, length)
 * -------------------------------------------------
 * Draws a triangle given the coordinates of the top vertex and length of the triangle
 * One line from top vertex at -120 deg
 * One line from top vertex at -60 deg
 * One line from left vertex at 0 deg
 */
void drawTriangle(double x, double y, double length)
{
    polarLine(x,y,length,-120);
    polarLine(x,y,length,-60);
    polarLine(x-length/2, y-length*HEIGHT_FACTOR, length, 0);
}

/**
 * Function: sierpinskiTriangle
 * Usage: sierpinskiTriangle(order, length);
 * -------------------------------------------------------
 * Calls sierpinskiIteration() if uses said so
 * otherwise calls sierpinskiRecursion()
 */
void sierpinskiTriangle(int order, int length)
{
    if (useIteration)
        sierpinskiIteration(order, length);
    else
        sierpinskiRecursion(order, length, length/2, length*HEIGHT_FACTOR);
}

/**
 * Function: sierpinskiRecusion
 * Usage: sierpinskiRecursion(10, 600, x,y)
 * ---------------------------------------------------
 * Draws a Sierpinski's Triangle of the given order and length with the topmost vertex at (x,y)
 * 
 * Base Case: order is 1
 *      - draw a simple triangle at the given (x,y)
 * Reduction: 
 *      - draw three triangles of half the length
 *      - one at topmost vertex
 *      - use the two bottom vertices of the last triangle at the top vertex of two triangles
 */
void sierpinskiRecursion(int order, int length, double x, double y)
{
    if (order == 1)
        drawTriangle(x, y, length);
    else
    {
        length /= 2;                //set length to half of original
        
        sierpinskiRecursion(order-1, length, x, y);
        sierpinskiRecursion(order-1, length, x - length/2, y - length*HEIGHT_FACTOR);
        sierpinskiRecursion(order-1, length, x + length/2, y - length*HEIGHT_FACTOR);
    }
}

/**
 * Function: sierpinskiIteration
 * Usage: sierpinskiIteration(order, length)
 * ----------------------------------------------------
 * Sierpinski's Triangle has a property that all vertices of the triangles must stem from previously used vertices
 * Construct a set of GPoints to prevent redundancy and add the topmost vertex as the initial vertex
 * 
 * Start: Using that vertex, draw a triangle 
 *
 * Repeat the following for order-1 times
 * 1. Draw a triangle half the length from the same top vertex
 * 2. Draw two more triangles from the bottom two vertices of the last triangle
 * 3. Repeat
 */
void sierpinskiIteration(int order, int length)
{
    Set<GPoint> vertices;
    vertices.add(*new GPoint(length/2, length*HEIGHT_FACTOR));
    drawTriangle(vertices.first().getX(), vertices.first().getY(), length);
    
    for (int i=0; i<order-1; i++)
    {
        length /= 2;
        foreach (GPoint point in vertices)
        {
            drawTriangle(point.getX(), point.getY(), length);
            
            GPoint* newPoint = new GPoint(point.getX() - length/2, point.getY() - length*HEIGHT_FACTOR);
            vertices.add(*newPoint);
            drawTriangle(newPoint->getX(), newPoint->getY(), length);
            
            newPoint = new GPoint(point.getX() + length/2, point.getY() - length*HEIGHT_FACTOR);
            vertices.add(*newPoint);
            drawTriangle(newPoint->getX(), newPoint->getY(), length);
        }
    }
}

/** 
 * Operator: < 
 * Usage: GPoint a < GPoint b
 * --------------------------------------------
 * Defines the "less than" operator for GPoints
 * When comparing coordinate points, x-coordinate takes priority
 * 
 * if (x1 < x2) then first GPoint is smaller, regardless of the y-coordinates
 * if (x1==x2) then check the y-coordinates
 */
bool operator < (const GPoint& a, const GPoint& b)
{
    if (a.getX() < b.getX())
        return true;
    
    if (a.getX()==b.getX() && a.getY()<b.getY())
        return true;
    
    return false;
}









