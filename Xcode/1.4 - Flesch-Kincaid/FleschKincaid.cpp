/*
 * File: FleschKincaid.cpp
 * ----------------------
 * Name: Bohui Moon
 * Section: [TODO: enter section leader here]
 * This file is the starter project for the Flesch-Kincaid problem.
 *
 * Flesh-Kincaid is the equation for calculating approx. grade level of a reading.
 * Equation is given by G(a, b, c) = C0 + C1(a/b) + C2(c/a)
 *      where a = word count, b = sentence count, c = syllable count
 *      constants C0 = -15.59, C1 = 0.39, C2 = 11.8
 *
 * Functions:
 *      getReadingGradeLevel() - master function sets up scanner and repeatedly calls necessary functions for getting grade levels of files
 *      setupTokenScanner() - sets up the TokenScanner to ignore spaces and ' as separate tokens
 *      getDebugPreference() - receives whether user wants debugging information
 *      openFileStream() - gets user input on file to open and sets scanner to read from openned stream
 *      getFileStatistics() - calculates the word/sent/syllable counts of file openned in scanner
 *      syllablesOf(word) - returns number of syllables of given word
 *      isVowel(letter) - returns whether given letter is a vowel
 *      fleshKincaid(word,sent,syl) - returns double grade level value from given word/sent/syllable counts
 *      debug(mssg) - displays debugging info if DEBUG is on
 * 
 * Works cited:
 *      cplusplus.com reference on ifstream ("http://www.cplusplus.com/reference/fstream/ifstream/ifstream/")
 *      cplusplus.com reference on pointers ("http://www.cplusplus.com/doc/tutorial/pointers/");
 */
 
#include <iostream>
#include <string>
#include "tokenscanner.h"
#include "console.h"
#include "strlib.h"
#include <fstream>
using namespace std;

/* Constants & Instance fields */
const double C0 = -15.59;
const double C1 = 0.39;
const double C2 = 11.8;

bool DEBUG = true;
TokenScanner scanner;
ifstream* infile;

/* Function Prototypes */
string getLine(string mssg = "");

void getReadingGradeLevel();
void setupTokenScanner();
bool getDebugPreference();
void openFileStream();
void getFileStatistics();
int syllablesOf(string word);
bool isVowel(string character);
double fleshKincaid(int wordCount, int sentCount, int syllableCount);
void debug(string message);

/* Code */

/**
 * Function: main
 * Usage: int main()
 * ----------------------
 * Calls fleshKincaid() function to calculate the level of various text files.
 */
int main() 
{
    getReadingGradeLevel();
    return 0;
}

/**
 * Function: readingLevel
 * Usage: readingLevel();i
 * ------------------------------
 * Sets up the scanner and ifstream for reading files.
 * Asks user whether to show DEBUG info or not
 * Indefinitely asks for a file name and shows statistics (word/sent/syllable count) 
 * and reading level of file.
 */
void getReadingGradeLevel()
{
    setupTokenScanner();
    DEBUG = getDebugPreference();
    
    while (true)
    {
        openFileStream();
        getFileStatistics();
    }
}

/**
 * Function: setupTokenScanner
 * Usage: setupTokenScanner();
 * ------------------------------
 * Sets up TokenScanner for reading text files.
 *      Regards apostrophie (') as part of a word
 *      Does not return " " as tokens
 *      Sets ifstream infile object as source of input
 */
void setupTokenScanner()
{
    scanner.addWordCharacters("/'");
    scanner.ignoreWhitespace();
}

/**
 * Function: getDebugPreference
 * Usage: DEBUG = getDebugPreferenc();
 * -----------------------------------------
 * Asks the user whether or not to show debugging information using getLine()
 * Checks whether answer was "yes" or "no"; calls itself if neither
 * Returns true for "yes" & false for "no"
 */
bool getDebugPreference()
{
    string answer = getLine("Show debug information? ");
    if ( !(answer == "yes" || answer == "no") ) //answer must be "yes" or "no"
    {
        cout << "Please enter yes or no" << endl;
        return getDebugPreference();
    }
    return (answer == "yes")? true:false; //return true if "yes"
}

/**
 * Function: openFileStream()
 * Usage: openFileStream();
 * ----------------------------------
 * Asks for a new file if entered file is not available.
 * Assumes that all files have at least 1 token;
 * therefore, asks for another file if scanner does not detect any tokens
 */
void openFileStream()
{
    bool fileOpened = false;
    while (!fileOpened)
    {
        
        scanner.setInput(*infile);
        if (!scanner.hasMoreTokens())
            cout << "Unable to open that file. Try again." << endl;
        else
            fileOpened = true;
    }
}

/**
 * Function: getFileStatistics
 * Usage: getFileStatistics();
 * -----------------------------------
 * Scans a file using TokenScanner and calculates the number of words/sent/syllables.
 * Goes through a file using scanner.hasMoreTokens()
 *      Increments sent count for "." / "!" / "?"
 *      Increments word count for any token that comes after "A" in lexographical order
 *              Increments syllable count according value given by syllablesOf()
 * Calls fleshKincaid() with the calculated information
 * 
 * Shows debugging information using debug()
 */
void getFileStatistics()
{
    int wordCount = 0;
    int syllableCount = 0;
    int sentCount = 0;
    
    while(scanner.hasMoreTokens())
    {
        string token = scanner.nextToken();
        int lexUpperOrder = token.substr(0,1).compare("A");
        int lexLowerOrder = token.substr(0,1).compare("a");
        
        debug("Token: [" + token + "] ");
        
        if (token == "." || token == "!" || token == "?") //sentences
        {
            sentCount++;
            debug("(end of sentence)");
        }
        else if ((0<=lexUpperOrder && lexUpperOrder<=25) || (0<=lexLowerOrder && lexLowerOrder<=25)) //words
        {
            wordCount++;
            syllableCount += syllablesOf(token);
            debug("(word; " + integerToString(syllablesOf(token)) + " syllables)");
        }
        
        debug("\n");
    }
    
    cout << "Words: " << wordCount << endl;
    cout << "Syllables: " << syllableCount << endl;
    cout << "Sentences: " << sentCount << endl;
    cout << "Grade level: " << fleshKincaid(wordCount, sentCount, syllableCount) << endl << endl;
}

/**
 * Function: syllablesOf
 * Usage: syllableCount += syllablesOf(word);
 * ------------------------------------------------
 * Counts the number of syllables the given word contains.
 * Syllables depends on the number of vowels.
 * But theses cases are not counted:
 *      - vowel comes after another vowel
 *      - vowel is "e" and is the last letter
 * If count is 0, returned as 1 under assumption that all word are at least 1 syllable
 *
 * Param: word - word to get count syllables of
 * Return: number of syllables of word
 */
int syllablesOf(string word)
{
    int count = 0;
    
    for (int i=0; i<word.length(); i++)
    {
        string character = word.substr(i,1);
        
        if(isVowel(character))
            if ( 
                 !(i!=0 && isVowel(word.substr(i-1,1)))  
                                &&
                 !(i==word.length()-1 && character=="e") 
               )
                count++;
    }
    
    if (count ==0) //assume that all words have at least one syllable
        count++;
        
    return count;
}

/**
 * Function: isVowel
 * Usage: isVowel(word.substr(0,1))
 * -----------------------------------------
 * Returns whether the given word is a vowel with hard coding
 * 
 * Precondition: letter is of length 1
 * Returns: true if a vowel (uppercase applies)
 */
bool isVowel(string letter)
{
    letter = toLowerCase(letter);
    if (letter == "a" || letter == "e" || letter == "i" || letter == "o" || letter == "u" || letter == "y")
            return true;
    return false;
}

/**
 * Function: fleshKincaid
 * Usage: fleshKincaid(word, sent, syllables);
 * -----------------------------------------------
 * Calculates and returns grade level with given word/sent/syllable counts
 * using the Flesh-Kincaid equation
 *
 * Param: wordCount - number of words in a file
 * Param: sentCount - number of sentences in a file
 * Param: syllableCount - number of syllables in a file
 * Return: grade level of file in double
 */
double fleshKincaid(int wordCount, int sentCount, int syllableCount)
{
    return C0 + (C1*wordCount/sentCount) + (C2*syllableCount/wordCount);
}

/**
 * Function: debug
 * Usage: debug("information");
 * ------------------------------------
 * Displays the given message if instance field DEBUG is set to true
 *
 * Param: message - debuggin info to display
 */
void debug(string message)
{
    if (DEBUG)
        cout << message;
}