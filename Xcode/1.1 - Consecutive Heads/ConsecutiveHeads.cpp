/*
 * File: ConsecutiveHeads.cpp
 * --------------------------
 * Name: Bohui Moon
 * Section: [TODO: enter section leader here]
 * This file is the starter project for the Consecutive Heads problem.
 *
 * Provides functions on flipping coins and matching a certian sequence
 * Note: All code assumes boolean value true as heads & false as tails
 * 
 * Functions:
 *      coinFlip() - replicates a coin flip by producing a true boolean value with 50% chance
 *      getThreeHeads() - calls coinFlip() until 3 consecutive heads are thrown
 *      getSequence() - asks user for a sequence of coins and flips until the sequence is matched
 *      fillSequenceArray(bool[], string) - given an boolean array & sequence of coins, fills the array according to the sequence
 */

#include <iostream>
#include <string>
#include "console.h"
#include "random.h"
using namespace std;

/* Function prototypes */
string getLine(string message);
string convertToCoin(bool value);
void getSequence();
void fillSequenceArray(bool sequence[], string sequenceStr);
void getThreeHeads();
string getLine(string message = "");

/**
 * Function: main
 * Usage: int main()
 * --------------------------
 * Calls getThreeHeads() to test how many flips it took to get 3 consecutive heads
 * or
 * getSequence() for any arbitray sequence of H & T inputed
 * 
 * returns 0 - system exit
 */
int main() 
{
    while (true) 
    {
        getSequence();
    }
    return 0;
}

/**
 * Function: convertToCoin
 * Usage: cout << convertToCoin(true) << endl;
 * -----------------------------------------
 * Converts boolean value to side of a coin as string according to a set rule
 *
 * Return: "heads" for true & "tails" for false
 */
string convertToCoin(bool heads)
{
    if (heads)
        return "heads";
    return "tails";
}

/**
 * Function: getSequence
 * Usage: getSequenc();
 * --------------------------------
 * Flips coins until sequence inputed by user is reached.
 * Does so by creating a boolean array representing the given sequence.
 * After each coin flip:
 *      matches sequence: index incremented
 *      does not match: index reset to 0
 * Sequence has been reached when index becomes equal to length of sequence string entered.
 *
 * Prints out total number of flips required to reach sequence
 */
void getSequence()
{
    string sequenceStr = getLine("Enter a sequence. Ex)HTHT \n");

    int length = sequenceStr.length();
    bool sequence[length];
    fillSequenceArray(sequence, sequenceStr);
    
    int totalFlips = 0;
    int index = 0;
    
    while (index < length)
    {
        bool heads = randomChance(.5);
        totalFlips++;
        
        if (sequence[index] == heads)
            index++;
        else
            index = 0;
            
        cout << convertToCoin(heads) << endl;
    }
    
    cout << "It took " << totalFlips << " flips to get " << sequenceStr << " heads in a row!" << endl;
}

/**
 * Function: fillSequenceArray
 * Usage: fillSequenceArray(sequence[], sequenceStr);
 * -------------------------------------------------------
 * Fills the given array so that it represents the given string, 
 * in which true represents "heads" & false represents "tails"
 *
 * Precondition: length of sequence[] = sequenceStr.length()
 * Param: sequence[] - boolean array to represent given sequence of coins flips
 * Param: sequenceStr - string of coin flips inputed by user
 */
void fillSequenceArray(bool sequence[], string sequenceStr)
{
    for (int i=0; i<sequenceStr.length(); i++)
    {
        if (sequenceStr.substr(i,1) == "H")
            sequence[i] = true; //true is heads for the purpose of this program
        else
            sequence[i] = false;
    }
}

/**
 * Function: getThreeHeads
 * Usage: getThreeHeads();
 * ----------------------------------
 * Continuously flips coins using coinFlip() until 3 consecutive heads are flipped.
 * Initializes totalFlips & consecutiveHeads to 0.
 * Every time a heads is flipped, consecutiveHeads is incremented; reset to 0 when tails is flipped.
 * While loop runs until consecutiveHeads hits 3; increments totalFlips every run.
 *
 * Displays final message of how many total flips it took to get 3 consecutive heads
 */
void getThreeHeads()
{
	int totalFlips = 0;
	int consecutiveHeads = 0;

	getLine("Flip!"); //pauses console before continuing on with coin flips
	while (consecutiveHeads < 3)
	{
        bool heads = randomChance(.5);
        totalFlips++;
        
		if (heads)
			consecutiveHeads++;
		else
			consecutiveHeads = 0;
            
        cout << convertToCoin(heads) << endl;
    }

	 cout << "It took " << totalFlips << " flips to get 3 heads in a row!" << endl;
}
