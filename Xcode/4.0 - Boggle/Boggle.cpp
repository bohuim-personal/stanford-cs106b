/*
 * File: Boggle.cpp
 * ----------------
 * Name: Bohui (Dennis) Moon
 * Section: Steve Cooper
 * This file is the main starter file for Assignment #4, Boggle.
 * 
 * Implements the Boggle game in a single file with help of gboggle.h 
 * Supports both (4x4) and (5x5) grid by using a BoggleBoard struct
 * Gives user option to choose between the sizes & whether or not to force configure.
 * 
 * Implemented graphical play method, so user can use the mouse to select words.
 */
 
#include <iostream>
#include <string>
#include "strlib.h"
#include "gBoggle.h"
#include "grid.h"
#include "gwindow.h"
#include "lexicon.h"
#include "random.h"
#include "simpio.h"
#include "gevents.h"
#include "ginteractors.h"
using namespace std;

/* Constants */
    const int BoggleBoard_WINDOW_WIDTH = 650;
    const int BoggleBoard_WINDOW_HEIGHT = 350;

    const int MIN_WORD_LENGTH = 4;
    const Lexicon lex("EnglishWords.dat");
    
    //for mouse implementation
    const int LEFT_MARGIN = 176;
    const int TOP_MARGIN = 51;
    const int BOARD_WIDTH = 204;
    
    const string STANDARD_CUBES[16]  = 
    {
        "AAEEGN", "ABBJOO", "ACHOPS", "AFFKPS",
        "AOOTTW", "CIMOTU", "DEILRX", "DELRVY",
        "DISTTY", "EEGHNW", "EEINSU", "EHRTVW",
        "EIOSST", "ELRTTY", "HIMNQU", "HLNNRZ"
    };
    const string BIG_BoggleBoard_CUBES[25]  = 
    {
        "AAAFRS", "AAEEEE", "AAFIRS", "ADENNN", "AEEEEM",
        "AEEGMU", "AEGMNN", "AFIRSY", "BJKQXZ", "CCNSTW",
        "CEIILT", "CEILPT", "CEIPST", "DDLNOR", "DDHNOT",
        "DHHLOR", "DHLNOR", "EIIITT", "EMOTTT", "ENSSSU",
        "FIPRSY", "GORRVW", "HIPRRY", "NOOTUW", "OOOTTU"
    };
    
/* Structs */
    /**
     * @struct: BoggleBoard
     * usage: BoggleBoard board(getBoardLength());
     * ---------------------------------------------
     * Simple grouping of actual char array to represent the game board
     * array to represent check letters
     * length of one side & size of board
     */
    struct BoggleBoard
    {
        //Instance variables
        Set<string> used;
        char* letterBoard;
        bool* checked;
        int length;
        int size;
        
        //constructor & deconstructor
        BoggleBoard(int rowLength)
        {
            length = rowLength;
            size = rowLength * rowLength;
            letterBoard = new char[size];
            checked = new bool[size];
        }
        ~BoggleBoard()
        {
            //delete when this instance of BoggleBoard is destroyed
            delete letterBoard;
            delete checked;
        }
    };
    
    /**
     * @struct: Position
     * usage: Position pos;
     * ---------------------------
     * Simple grouping of row & col to represent 
     * a position on the game baord
     */
    struct Position
    {
        int row;
        int col;
    };
    
/* Function prototypes */
    string getLine(string mssg);
    string toUpperCase(string original);

    void welcome();
    void giveInstructions();
    void playBoggle(GWindow& gw);
    int getBoardLength();
    void fillBoard(BoggleBoard& board);
        void shuffle(BoggleBoard& board);
    void playerTurn(BoggleBoard& board);
        void TUIplayer(BoggleBoard& board);
        void GUIplayer(BoggleBoard& board);
            void selection(BoggleBoard& board, Vector<int> &path);
        bool isValidInput(string& word, Set<string> &used);
        bool canBeFormed(string word, BoggleBoard& board, Vector<Position> &path);
        bool canBeFormedAround(string word, BoggleBoard& board, Position pos, Vector<Position> &path);
        void resetchecked(BoggleBoard& board);
        bool isValidPosition(int length, int row, int col);
    void computerTurn(BoggleBoard& board);
        void checkWordsAtPosition(string word, BoggleBoard& board, Position pos);

/* Main program */
int main() 
{
    /* setup */
    GWindow gw(BoggleBoard_WINDOW_WIDTH, BoggleBoard_WINDOW_HEIGHT);
    initGBoggle(gw);
    welcome();
    giveInstructions();
    
    playBoggle(gw);
    
    gw.close();
    return 0;
}

/**
 * Function: welcome
 * Usage: welcome();
 * -----------------
 * Print out a cheery welcome message.
 */
void welcome() 
{
    cout << "Welcome!  You're about to play an intense game ";
    cout << "of mind-numbing BoggleBoard.  The good news is that ";
    cout << "you might improve your vocabulary a bit.  The ";
    cout << "bad news is that you're probably going to lose ";
    cout << "miserably to this little dictionary-toting hunk ";
    cout << "of silicon.  If only YOU had a gig of RAM..." << endl;
}

/**
 * Function: giveInstructions
 * Usage: giveInstructions();
 * --------------------------
 * Print out the instructions for the user.
 */
void giveInstructions() 
{
    cout << endl;
    string answer = getLine("Do you need instructions? ");
    if (answer == "yes")
    {
        cout << endl;
        cout << "The BoggleBoard board is a grid onto which I ";
        cout << "I will randomly distribute cubes. These ";
        cout << "6-sided cubes have letters rather than ";
        cout << "numbers on the faces, creating a grid of ";
        cout << "letters on which you try to form words. ";
        cout << "You go first, entering all the words you can ";
        cout << "find that are formed by tracing adjoining ";
        cout << "letters. Two letters adjoin if they are next ";
        cout << "to each other horizontally, vertically, or ";
        cout << "diagonally. A letter can only be used once ";
        cout << "in each word. Words must be at least four ";
        cout << "letters long and can be counted only once. ";
        cout << "You score points based on word length: a ";
        cout << "4-letter word is worth 1 point, 5-letters ";
        cout << "earn 2 points, and so on. After your puny ";
        cout << "brain is exhausted, I, the supercomputer, ";
        cout << "will find all the remaining words and double ";
        cout << "or triple your paltry score." << endl;
        cout << "Hit return when you're ready...";
        getLine();
    }
    else if (answer == "no") {}
    else
    {
        cout << "Please answer yes or no";
        giveInstructions();
    }
}

/**
 * @function: playBoggle
 * usage: playBoggle(gw);
 * ------------------------------------
 * Plays a round of boggle until users says "no" to play again.
 * 
 * Creates a BoggleBoard struct using user-preferred size
 * Draws & fills the board according to user's answers.
 * Allows user to play, then calls computer when user is done.
 * Asks for replay or not.
 */
void playBoggle(GWindow& gw)
{
    string cont = "yes";
    while (cont == "yes")
    {
        BoggleBoard board(getBoardLength());
        drawBoard(board.length, board.length);
        fillBoard(board);
        
        playerTurn(board);
        computerTurn(board);
        
        cout << endl;
        bool valid = false;
        while (!valid)
        {
            cont = getLine("Play again? ");
            if (cont != "yes" && cont != "no")
                cout << "Please answer yes or no." << endl;
            else 
                valid = true;
        }
        gw.clear();
    }
}

/**
 * Function: getboardSize
 * Usage: int length = getBoardSize();
 * --------------------------------------
 * Asks user if (4x4) or (5x5) and returns the board row length
 * 
 * @return length of chosen board
 */
int getBoardLength()
{
    cout << endl;
    cout << "You can choose standard BoggleBoard (4x4) or Big BoggleBoard (5x5)" << endl;
    string answer = getLine("Would you like Big BoggleBoard? ");
    
    if (answer == "yes")
        return 5;
    else if (answer == "no")
        return 4;
    else 
    {
        cout << "Please answer yes or no";
        return getBoardLength();
    }
}

/** 
 * @function: fillBoard
 * usage: fillBoard(board);
 * -------------------------------------
 * Asks user whether to force configure or not.
 * If yes, asks for appropriate length string,
 * otherwise calls shuffle() 
 * 
 * @param board - to fill
 */
void fillBoard(BoggleBoard& board)
{
    cout << endl;
    string answer = getLine("Do you want to custom configure the board? ");
    
    if (answer == "yes")
    {
        cout << endl;
        cout << "Enter a " << board.size << "string to identify which letters you want on the cubes." << endl;
        cout << "The first " << board.length << " letters are the cubes on the top row from left to right, and so on.";
        string config = "";
        
        bool valid = false;
        while (!valid)
        {
            cout << endl;
            config = toUpperCase(getLine("Enter the string: "));
            if (config.length() == board.size)
                valid = true;
            else 
                cout << "Length does not match." << endl;
        }
        
        for (int i=0; i<board.size; i++)
        {
            board.letterBoard[i] = config[i];
            labelCube(i/board.length, i%board.length, config[i]);
        }
    }
    else if (answer == "no")
        shuffle(board);
    else
    {
        cout << "Please answer yes or no" << endl;
        return fillBoard(board);
    }
}

/**
 * Function: shuffle
 * Usage: shuffle(size);
 * -------------------------------
 * Given a BoggleBoard, shuffles the appropriate cubes and inserts them into the board
 * 
 * Copys the cubes into a vector, 
 * chooses a random cube & a random letter from the cube
 * inserts that letter into the board in order 
 * and removes the cube from copy vector
 * 
 * @param board - BoggleBoard to fill randomly
 */
void shuffle(BoggleBoard& board)
{
    Vector<string> tempCopy;
    for (int i=0; i<board.size; i++)
    {
        if (board.size == 16) tempCopy.add(STANDARD_CUBES[i]);
        else tempCopy.add(BIG_BoggleBoard_CUBES[i]);
    }
    
    for (int i=0; i<board.size; i++)
    {
        int randIndex = randomInteger(0,tempCopy.size()-1);
        
        string cube = tempCopy[randIndex];
        char chosen = cube[randomInteger(0,cube.length()-1)];
        board.letterBoard[i] = chosen;
        labelCube(i/board.length, i%board.length, chosen);
        
        tempCopy.remove(randIndex);
    }
}

/**
 * @function: playerTurn
 * usage: playerTurn(board)
 * -------------------------------
 * Asks the user whether they want to play with the mouse or type
 * calls GUIplayer() or TUIplayer accordingly
 *
 * @param board - BoggleBoard to pass on
 */
void playerTurn(BoggleBoard& board)
{
    cout << endl;
    
    cout << "You can use your mouse to drag and select the letters instead of typing." << endl;
    cout << "Then, simply click on nothing to finish." << endl;
    string answer;
    
    bool validAnswer = false;
    while (!validAnswer)
    {
        answer = getLine("Would you like to use the mouse? ");
        if (!(answer == "yes" || answer == "no")) 
            cout << "Please answer yes or no." << endl;
        else
            validAnswer = true;
    }
    
    if (answer == "yes")
        GUIplayer(board);
    else
        TUIplayer(board);
}

/**
 * @function: TUIplayer
 * usage: TUIplayer(board)
 * ------------------------------
 * User plays boggle by typing the words they see.
 * Asks the user for an input until the input is blanks.
 * Checks whether input is valid & can be formed on the board
 *      if it can: add to used words & record the score & briefly hightlight
 * Clear path & pathed after every input
 *
 * @param board - BoggleBoard to check inputs from
 */
void TUIplayer(BoggleBoard& board)
{
    cout << endl; //formatting
    
    Vector<Position> path;
    
    path.clear();
    resetchecked(board);
    string input = toUpperCase(getLine("Enter a word: "));
    
    while (input != "")
    {
        if (isValidInput(input, board.used) && canBeFormed(input, board, path))
        {
            board.used.add(input);
            recordWordForPlayer(input, HUMAN);
            
            foreach(Position pos in path)
            {
                highlightCube(pos.row, pos.col, true);
                pause(200);
            }
            foreach(Position pos in path)
            highlightCube(pos.row, pos.col, false);
            path.clear();
        }
        
        path.clear();
        resetchecked(board);
        input = toUpperCase(getLine("Enter a word: "));
    }
}

/** 
 * @function: GUIplayer
 * usage: GUIplayer(board)
 * ------------------------------
 * User plays by selecting words with a mouse by dragging until a black click is entered.
 * Creates a Vector of indices already selected & is passed on to selection()
 * Combines the letters at the indices added by selection() to check if valid
 * If it is records for player & addes to board's used words.
 *
 * @param board - BoggleBoard to check from
 */
void GUIplayer(BoggleBoard& board)
{
    Vector<int> path;
    path.add(0); //filler
    
    while (!path.isEmpty())
    {
        path.clear();
        
        selection(board, path);
        
        string word;
        foreach(int index in path)
        {
            word += board.letterBoard[index];
            highlightCube(index/board.length, index%board.length, false);
        }
        if (isValidInput(word, board.used))
        {
            recordWordForPlayer(word, HUMAN);
            board.used.add(word);
        }
    }
}

/**
 * @function: selection
 * usage: selection(board, path)
 * ------------------------------------------
 * Allows users to graphically select words by using GMouseEvents.
 * Creates a Set<int> to prevent repeated selection & tracks prevIndex.
 * Calculates cube box width for this BoggleBoard.
 * Waits until a mouse button is pressed before entering the loop.
 * 
 * Adjust the (x,y) of mouse due to graphics margin - calculate (row,col) & index from the info.
 * Mark the valid selection box smaller to allow diagonal selection.
 * Prevent selecting again & allow user to de-select whatever was selected.
 * Highlight the selection & add to path.
 * Once user releases press, exit method.
 */
void selection(BoggleBoard& board, Vector<int> &path)
{
    Set<int> usedIndex;
    int prevIndex = -1;
    
    //cube width depends on size of board, so is not a global constant
    const int CUBE_WIDTH = BOARD_WIDTH/board.length;
    
    GMouseEvent e;
    while (e.getEventType() != MOUSE_PRESSED)  //wait until mouse is pressed
        e = waitForEvent(MOUSE_EVENT);
    
    while (e.getEventType() != MOUSE_RELEASED) //do while mouse is dragged, which is equal to mouse not released
    {
        int adjustedX = e.getX()-LEFT_MARGIN;  //account for where the board is located graphically
        int adjustedY = e.getY()-TOP_MARGIN;
        
        int row = adjustedY/CUBE_WIDTH;         //get row by dividing y-position by width of a cube box
        int col = adjustedX/CUBE_WIDTH;         //similarly 
        int index = (row * board.length) + col; //calculate board array index
        if (prevIndex < 0) prevIndex = index; //for the first click
        
        //prevent processing (row,col) off the board
        //put dead zone margins around cube box, so that going diagonally works 
        if (0 <= row && row < board.length && 0 <= col && col < board.length && 
            (row*CUBE_WIDTH+5 <= adjustedY && adjustedY <= row*CUBE_WIDTH +CUBE_WIDTH-5) && 
            (col*CUBE_WIDTH+5 <= adjustedX && adjustedX <= col*CUBE_WIDTH +CUBE_WIDTH-5))
        {
            int prevRow = prevIndex/board.length;
            int prevCol = prevIndex%board.length;
        
            //if cube was already selected
            if (usedIndex.contains(index))
            {
                //and it's the one before the last selection
                if (index!=prevIndex && path[path.size()-2]==index)
                {
                    //remove the current one from selection
                    highlightCube(prevRow, prevCol, false);
                    usedIndex.remove(prevIndex);
                    path.remove(path.size()-1);
                    prevIndex = index;
                }
                
                //if not previously selected, do nothing
            }
            else
            {
                //do not add to selection unless directly next to previous selection
                if ( prevRow-1 <= row && row <= prevRow+1 && prevCol - 1 <= col && col <= prevCol + 1 )
                {
                    highlightCube(row, col, true);
                    usedIndex.add(index);
                    path.add(index);
                    prevIndex = index;
                }
            }
        }
        
        e = waitForEvent(MOUSE_EVENT);
    }
}

/** 
 * @function: isValidInput
 * usage: if (isValidInput(input, used))
 * -------------------------------------------
 * Checks whether the given input is valid
 *      - is longer than 3 letters
 *      - is an English word
 *      - not already used
 * 
 * @param input - from user to check
 * @param used - set of valid words already entered by user
 * @return true if passes above conditions
 */
bool isValidInput(string& word, Set<string>& used)
{
    if (word.length() < MIN_WORD_LENGTH)
    {
        cout << "Minimum word length is 4!" << endl;
        return false;
    }
    
    if (!lex.contains(word))
    {
        cout << "That's not a word!" << endl;
        return false;
    }
    
    if (used.contains(word))
    {
        cout << "You already guessed that word!" << endl;
        return false;
    }
    
    return true;
}

/**
 * @function: canBeFormed
 * usage: if (canBeFormed(word, board))
 * ------------------------------------------
 * Goes through the entire board to pick out all positions where first letter mathces.
 * For each of those positions, use recursive backtracking
 *      - get the row&col form
 *      - call canBeFormedAround()
 *      - if any of them are true, return true
 * 
 * @param word - to check first letter of
 * @param board - reference to BoggleBoard to check if word can be formed
 * @param path - reference to path of a valid word
 * @return false if first letter is not found; then return whatever canBeFormedAround() returns
 */
bool canBeFormed(string word, BoggleBoard& board, Vector<Position> &path)
{
    Position pos;
    for (int i=0; i<board.size; i++)
        if (board.letterBoard[i] == word[0])
        {
            pos.row = i/board.length;
            pos.col = i%board.length;
            if (canBeFormedAround(word, board, pos, path))
                return true;
            resetchecked(board);
        }
    cout << "That word can't be formed!!" << endl;
    return false;
}

/**
 * @function: canBeFormedAround
 * usage: return canBeForemdAround(word.substr(1), board, r,c);
 * ---------------------------------------------------------------
 * Returns whether the given word can be made by 
 * searching around the given position using recursive backtracking
 *
 * Base Case: false when any of the below are false
 *      - Position is not on board
 *      - Position has already been checked
 *      - Position's letter does not match first letter
 *
 * Reduction:
 *      - start with the (row,col) 1 behind the given
 *      - check until (row,col) 1 after the given
 *      - if any recursive call is true
 *          - return true
 *
 * @param word - to check if possible
 * @param board - to check from if word is possible
 * @param pos - position on board to check around
 * @return true at base case, false if no match is found
 */
bool canBeFormedAround(string word, BoggleBoard& board, Position pos, Vector<Position> &path)
{
    int index = (pos.row * board.length) + pos.col; //conversion to index from row&col
    if ( 
         (!isValidPosition(board.length, pos.row, pos.col)) ||
         board.checked[index]                               ||
         board.letterBoard[index] != word[0]                
       )
        return false;
     
    path.add(pos);
    board.checked[index] = true;
    
    if (word.length() == 1)
        return true;
    
    //check around the given position
    Position newPos;
    for (int r = pos.row-1; r < pos.row+2; r++)
        for (int c = pos.col-1; c < pos.col+2; c++) 
        {
            newPos.row = r;
            newPos.col = c;
            
            if (canBeFormedAround(word.substr(1), board, newPos, path))
                return true;
        }
    path.remove(path.size()-1);
    return false;
}

/**
 * @function: resetchecked
 * usage: resetchecked(board)
 * -------------------------------------
 * Given a BoggleBoard, resets all values in the checked to false
 * 
 * @param board - BoggleBoard to reset its checked
 */
void resetchecked(BoggleBoard& board)
{
    for (int i=0; i<board.size; i++)
        board.checked[i] = false;
}

/**
 * @function: isValidPosition
 * usage: if (isValidPosition(board.length, r, c))
 * -----------------------------------------------------
 * Returns whether the given position is valid within 
 * the size of the current board
 * 
 * @param length - of the board
 * @param row - to check
 * @param col - col to check
 */
bool isValidPosition(int length, int row, int col)
{
    if (0 <= row && row < length &&
        0 <= col && col < length)
        return true;
    return false;
}

/**
 * @function: computerTurn
 * usage: computerTurn(board)
 * ----------------------------------
 * Exhaustively checks all possible words
 * by calling checkWordsAtPosition() at every (row,col)
 *
 * @param board - BoggleBoard to check words from
 */
void computerTurn(BoggleBoard& board)
{
    Position pos;
    for (int i=0; i<board.size; i++)
    {
        pos.row = i/board.length;
        pos.col = i%board.length;
        
        string word(1, board.letterBoard[i]);
        checkWordsAtPosition(word, board, pos);
        resetchecked(board);
    }
}

/**
 * @function: checkWordsAtPosition
 * usage: checkWordsAtPosition(word, board, pos);
 * --------------------------------------------------
 * Recursively checks all words that could stem from the given position
 * 
 * Base Case: given word is a word, longer than 4 letters, and was not already used
 *      - add as used word & record for computer
 * Baes Case: word is not a prefix to anything
 *      - nothing
 * Reduction:
 *      - mark as checked
 *      - check around the given position 
 *      - if position is valid & not already checked
 *          - recursively check from that position
 *      - unmark as checked
 * 
 * @param word - so far made by connecting letters
 * @param board - to make words from
 * @param pos - to start as starting position
 */
void checkWordsAtPosition(string word, BoggleBoard& board, Position pos)
{
    if (word.length() > MIN_WORD_LENGTH && lex.contains(word) && !board.used.contains(word))
    {
        board.used.add(word);
        recordWordForPlayer(word, COMPUTER);
    }
        
    if (lex.containsPrefix(word))
    {
        board.checked[(pos.row * board.length) + pos.col] = true;
    
        Position newPos;
        for (int r = pos.row-1; r < pos.row+2; r++)
            for (int c = pos.col-1; c < pos.col+2; c++) 
            {
                int index = (r*board.length) + c;
                newPos.row = r;
                newPos.col = c;
                
                if (isValidPosition(board.length, r, c) && !board.checked[index])
                    checkWordsAtPosition(word + board.letterBoard[index], board, newPos);
            }
        board.checked[(pos.row * board.length) + pos.col] = false;
    }
}