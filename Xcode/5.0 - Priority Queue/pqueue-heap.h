/**
 * File: pqueue-heap.h
 * ---------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 */
#ifndef PQueue_Heap_Included
#define PQueue_Heap_Included

#include <iostream>
#include <string>
using namespace std;

/**
 * Class: HeapPriorityQueue
 * ------------------------------------
 * A class representing a priority queue backed by an binary heap.
 * Binary heap has an underlying vector, which is backed by an dynamic array.
 * However this class directly manages an dynamic array.
 *
 * Keep cell 0 as empty cell.
 * Enqueue to the end of the array, double array whenever full.
 * Simply dequeue element at the top element of the heap.
 * Bubble up & down to keep heap sorted.
 */
class HeapPriorityQueue 
{
    public:
        /* Constructs a new, empty priority queue backed by a binary heap. */
        HeapPriorityQueue();
	
        /* Cleans up all memory allocated by this priority queue. */
        ~HeapPriorityQueue();
        
        /* Returns the number of elements in the priority queue. */
        int size();
        
        /* Returns whether or not the priority queue is empty. */
        bool isEmpty();
        
        /* Enqueues a new string into the priority queue. */
        void enqueue(string value);
        
        /* Returns, but does not remove, the lexicographically first string in the priority queue. */
        string peek();
	
        /* Returns and removes the lexicographically first string in the priority queue. */
        string dequeueMin();
    
    private:
        string* heap;
        int numElements;
        int length;
        
        void doubleLength();
        void bubbleUp(int index);
        void bubbleDown(int index);
        int smallerChildIndex(int index);
};

#endif
