/**
 * File: pqueue-linkedlist.cpp
 * --------------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 * 
 * Implements the member methods for the LinkedListPriorityQueue
 */
 
#include "pqueue-linkedlist.h"
#include "error.h"

/**
 * Constructor: LinkedListPriorityQueue 
 * Usage: LinkedListPriorityQueue *lpq = new LinkedListPriorityQueue();
 * ----------------------------------------------------------------------
 * Creates a new LinkedListPriorityQueue.
 * Initially sets first & last to null,
 * numElements to 0
 */
LinkedListPriorityQueue::LinkedListPriorityQueue() 
{
    first = NULL;
    last  = NULL;
    numElements = 0;
}

/**
 * Deconstruct: ~LinkedListPriorityQueue 
 * Usage: delete lpq;
 * --------------------------------------------------
 * De-allocates all Nodes in the Linked List before deleting the priority queue.
 * Keeps track of a node and one ahead of it.
 * Stores the one ahead, deletes the current, sets current to next.
 */
LinkedListPriorityQueue::~LinkedListPriorityQueue() 
{
	Node* current = first;
    Node* next;
    for (int i=0; i<size(); i++)
    {
        next = current->next;
        delete current;
        current = next;
    }
}

/**
 * Method: size
 * Usage: lpq->size()
 * ---------------------------------
 * Returns the number of elements in the queue (nodes)
 * 
 * @return numElements - number of currently enqueued elements
 */
int LinkedListPriorityQueue::size() 
{
	return numElements;
}

/**
 * Method: isEmpty
 * Usage: lpq->isEmpty()
 * ---------------------------------
 * Returns whether the queue is empty.
 * 
 * @return true if size is 0
 */
bool LinkedListPriorityQueue::isEmpty() 
{
	return size() == 0;
}

/**
 * Method: enqueue
 * Usage: lpq->enqueue("element");
 * ---------------------------------
 * Enqueues the given string into the queue.
 * 
 * Creates a new node with the given value.
 * if queue is empty
 *      - set first & last to the new node
 * otherwise
 *      - traverse the Linked List to find the position
 *      * because the Linked List is singly-linked, two pointers are used
 * increment numElements
 */
void LinkedListPriorityQueue::enqueue(string value) 
{
	Node* newNode = new Node(value);
    if (isEmpty())
    {
        first = newNode;
        last = newNode;
    }
    else
    {
        Node* prev = NULL;
        Node* current = first;
        while (current != NULL && current->value < value)
        {
            prev = current;
            current = current->next;
        }
            
        if (prev == NULL)
            first = newNode;
        else
            prev->next = newNode;
        
        newNode->next = current;
    }
    numElements++;
}

/**
 * Method: peek
 * Usage: lpq->peek()
 * ---------------------------------
 * Returns but does not remove the first element
 * 
 * @exception isEmpty - cannot peek at an empty queue
 * @return smallest element of the queue (value of first node)
 */
string LinkedListPriorityQueue::peek() 
{
    if (isEmpty())
        error("No Such Element");
        
	return first->value;
}

/**
 * Method: dequeueMin
 * Usage: lpq->dequeueMin()
 * ---------------------------------
 * Returns and removes the minimum element.
 * 
 * Stores the value of the first node and reset first to the second node.
 * Delete the old first node by deleting temp pointer.
 * If no elements are left, set last as NULL.
 * Decrement numElements
 * 
 * @exception isEmpty - cannot peek at an empty queue
 * @return smallest element in the queue (value of first node)
 */
string LinkedListPriorityQueue::dequeueMin() 
{
    if (size() == 0)
        error("No Such Element");
        
    string val = first->value;
    Node* temp = first;
    first = temp->next;
    delete temp;

    if (isEmpty())
        last = NULL;
    numElements--;
	return val;
}

