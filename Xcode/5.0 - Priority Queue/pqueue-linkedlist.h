/**
 * File: pqueue-linkedlist.h
 * ------------------------------------------
 * Author: Bohui Moon 
 * Section: Steve Cooper
 */
#ifndef PQueue_LinkedList_Included
#define PQueue_LinkedList_Included

#include <string>
using namespace std;

/**
 * Class: LinkedListPriorityQueue
 * -----------------------------------
 * A Priority Queue backed by a sorted singly-linked Linked List.
 * Instead of a using an pre-implemented Linked List,
 * struct Node is defined, and managed by this class.
 * 
 * Enqueues elements at the sorted position and 
 * dequeues from the front of the Linked List.
 * 
 * Class keeps track of first & last nodes, and the number of nodes
 */
class LinkedListPriorityQueue 
{
    public:
        /* Constructs a new, empty priority queue backed by a sorted linked list. */
        LinkedListPriorityQueue();
	
        /* Cleans up all memory allocated by this priority queue. */
        ~LinkedListPriorityQueue();
        
        /* Returns the number of elements in the priority queue. */
        int size();
        
        /* Returns whether or not the priority queue is empty. */
        bool isEmpty();
        
        /* Enqueues a new string into the priority queue. */
        void enqueue(string value);
        
        /* Returns, but does not remove, the lexicographically first string in the priority queue. */
        string peek();
        
        /* Returns and removes the lexicographically first string in the priority queue. */
        string dequeueMin();
	
    private:
        /**
         * Struct: Node
         * -------------------
         * Node stores a string value & pointer to next node.
         */
        struct Node
        {
            Node* next;
            string value;
            
            /**
             * Constructor: Node
             * Usage: Node* n = new Node("element");
             * --------------------------------------
             * Value is set to the given string val
             * Next is initially NULL, before explicitly set
             */
            Node(string val)
            {
                next = NULL;
                value = val;
            }
        };
        
        Node* first;
        Node* last;
        int numElements;
};

#endif
