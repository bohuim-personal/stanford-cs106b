/**
 * File: pqueue-vector.cpp
 * ---------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 * 
 * Implements the member methods for the VectorPriorityQueue
 */
 
#include "pqueue-vector.h"
#include "error.h"

/**
 * Constructor: VectorPriorityQueue
 * Usage: VectorPriorityQueue* vpq = new VectorPriorityQueue();
 * -----------------------------------
 * Makes a new Vector backed PriorityQueue
 * 
 * Creates a new underlying vector & initializes numElements to 0 
 */
VectorPriorityQueue::VectorPriorityQueue() 
{
	list = new Vector<string>;
}

/**
 * Deconstructor: ~VectorPriorityQueue
 * Usage: delete vpq;
 * -------------------------------
 * Before PriorityQueue is deleted,
 * de-allocates the memoory for the underlying vector
 */
VectorPriorityQueue::~VectorPriorityQueue() 
{
	delete list;
}

/** 
 * Method: size
 * Usage: vpq->size()
 * ---------------------------
 * Returns the number of actual elements in the queue
 * 
 * @return numElements - number of elements currently enqueued
 */
int VectorPriorityQueue::size() 
{
	return list->size();
}

/**
 * Method: isEmpty
 * Usage: vpq->isEmpty()
 * -----------------------------
 * Returns whether the queue is empty
 * 
 * @return true if no elements in the vector
 */
bool VectorPriorityQueue::isEmpty() 
{
	return size() == 0;
}

/**
 * Method: enqueue
 * Usage: vpq->enqueue("element");
 * -------------------------------
 * Adds the given string obj into the vector
 * 
 * @param value - element to insert into the vector
 */
void VectorPriorityQueue::enqueue(string value) 
{
    list->add(value);
}

/**
 * Method: peek
 * Usage: vpq->peek()
 * -----------------------------
 * Returns but does not remove the minimum element.
 * Traverses through the vector to find the lexiographically smallest element
 * 
 * @return smallest element in the queue
 */
string VectorPriorityQueue::peek() 
{
	return (*list)[minIndex()];
}

/**
 * Method: dequeueMin
 * Usage: vqp->dequeueMin()
 * -----------------------------
 * Returns and removes the minimum element.
 * Gets the index of the min element & copies the element
 * Then removes the element from the vector
 * 
 * @return element - minimum element in the queue
 */
string VectorPriorityQueue::dequeueMin() 
{
    int index = minIndex();
    string element = (*list)[index];
    list->remove(index);
	return element;
}

/**
 * Method: minIndex
 * Usage: int index = minIndex();
 * -------------------------------
 * Returns the index of the minimum element in the vector.
 * Traverses the vector and keeps track of 
 * min index & element to compare
 * 
 * @exception isEmpty - cannot traverse empty queue
 * @return min - index of the smallest element
 */
int VectorPriorityQueue::minIndex()
{
    if (isEmpty())
        error("No Such Element");
        
    int min = 0; 
    string element = (*list)[0];
    for (int i=1; i<size(); i++)
        if ((*list)[i] < element)
        {
            element = (*list)[i];
            min = i;
        }
    return min;
}