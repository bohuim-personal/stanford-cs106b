/**
 * File: pqueue-doublylinkedlist.h
 * --------------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 */
#ifndef PQueue_DoublyLinkedList_Included
#define PQueue_DoublyLinkedList_Included

#include <string>
using namespace std;

/**
 * Class: DoublyLinkedListPriorityQueue
 * -------------------------------------------
 * A class representing a priority queue backed by an unsorted doubly-linked list.
 * 
 * Enqueues to the end of the Linked List.
 * Traverses through the Linked List to the find the smallest element.
 */
class DoublyLinkedListPriorityQueue 
{
    public:
        /* Constructs a new, empty priority queue backed by a doubly-linked list. */
        DoublyLinkedListPriorityQueue();
        
        /* Cleans up all memory allocated by this priority queue. */
        ~DoublyLinkedListPriorityQueue();
        
        /* Returns the number of elements in the priority queue. */
        int size();
        
        /* Returns whether or not the priority queue is empty. */
        bool isEmpty();
        
        /* Enqueues a new string into the priority queue. */
        void enqueue(string value);
        
        /* Returns, but does not remove, the lexicographically first string in the priority queue. */
        string peek();
        
        /* Returns and removes the lexicographically first string in the priority queue. */
        string dequeueMin();
    
    private:
        /**
         * struct: Node
         * -----------------------
         * Node keeps tract of value, prev & next node.
         */
        struct Node 
        {
            /**
             * Constructor: Node
             * Usage: Node* n = new Node("element");
             * ---------------------------------------
             * Creates a new doubly-linked node.
             * Sets value to given val, and prev & next to NULL.
             */
            Node(string val)
            {
                value = val;
                prev = NULL;
                next = NULL;
            }
            
            string value;
            Node* prev;
            Node* next;
        };
        
        Node* first;
        Node* last;
        int numElements;
        
        Node* getMinNode();
};

#endif
