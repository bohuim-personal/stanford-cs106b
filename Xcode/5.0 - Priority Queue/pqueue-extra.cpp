/**
 * File: pqueue-extra.h
 * ------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 */
 
#include "pqueue-extra.h"
#include "error.h"

/**
 * constructor: ExtraPriorityQueue 
 * usage: ExtraPriorityQueue *epq = new ExtraPriorityQueue();
 * ---------------------------------------------------
 * Creates a PriorityQueue backed by a Binomial Heap
 * 
 * Initially adds one null cell and sets numElements to 0
 */
ExtraPriorityQueue::ExtraPriorityQueue() 
{
    length = 1;
    list = new Node*[length];
    list[0] = NULL;
    
    numElements = 0;
}

/**
 * deconstructor: ~ExtraPriorityQueue
 * usage: delete epq
 * -------------------------------
 */
ExtraPriorityQueue::~ExtraPriorityQueue() 
{
    delete[] list;
}

/**
 * @method: size
 * usage: epq->size()
 * -------------------------------
 * Returns the number of element in the queue
 *
 * @return numElements - number of currenlty enqueued elements
 */
int ExtraPriorityQueue::size() 
{
	return numElements;
}

/**
 * @method: isEmpty
 * usage: epq->isEmpty()
 * -------------------------------
 * Returns whehter queue is empty
 *
 * @return true if size is 0
 */
bool ExtraPriorityQueue::isEmpty() 
{
	return size() == 0;
}

/**
 * @method: enqueue
 * usage: epq->enqueue("element")
 * -------------------------------
 * Enqueues the given value to the queue
 * 
 * If Vector does not have enough cells, doubleLenght()
 * insert() from position 0, the new node containing the given value
 * increment numElements.
 *
 * @param value - to enqueue 
 */
void ExtraPriorityQueue::enqueue(string value) 
{
	insert(0, new Node(value));
    numElements++;
}

/**
 * @method: peek
 * usage: epq->peek()
 * -------------------------------
 * Returns but does not remove the smallest value
 * 
 * Traverse the vector to get the heap with the 
 * smallest root value
 *
 * @exception isEmpty - cannot peek from empty queue
 * @return minVal - smallest value in the queue
 */
string ExtraPriorityQueue::peek() 
{
    return list[minIndex()]->value;
}

/**
 * @method: dequeueMin
 * usage: epq->dequeueMin()
 * -------------------------------
 * Returns and removes the smallest value 
 *
 * Traverses the heap to find the smallest value and
 * the index of the node containing it.
 * Retrieve the Node & set the position as empty.
 * If it has any children (any index greater than 0)
 *      - merge its children with current heap
 * Delete the node & decrement numElements
 *  
 * @exception isEmpty - cannot dequeue from empty queue
 * @return minVal - smallest value in the queue
 */
string ExtraPriorityQueue::dequeueMin() 
{
	int index = minIndex();
    Node* minNode = list[index];
    string minVal = minNode->value;
    
    list[index] = NULL;
    if (index > 0)
        merge(minNode->children);
    delete minNode;
    
    numElements--;
	return minVal;
}

/**
 * @method: insert
 * usage: insert(node->order, node)
 * -------------------------------
 * Recursively adds the given Node to the heap.
 * Given the heap and its order, tries to insert into list from that order.
 * 
 * Base Case: list[index] is empty
 *      - add the heap
 * Reduction:
 *      - merge the new node with the original node
 *      - pass it onto the next index
 * 
 * @param index - to try adding to 
 * @param newHeap - node of heap to insert to list
 */
void ExtraPriorityQueue::insert(int index, Node* newHeap)
{
    if (index >= length)
        doubleLength();
    
    Node* oldHeap = list[index];
    if (oldHeap == NULL)
        list[index] = newHeap;
    else
    {
        list[index] = NULL;
        insert(index+1, mergeNodes(oldHeap, newHeap));
    }
}

/**
 * @method: mergeNodes
 * usage: mergeNodes(oldHeap, newHeap)
 * ------------------------------------
 * Given two heap root nodes, combines them.
 * 
 * If either is NULL, returns the non-NULL pointer.
 * Otherwise, add the node with the bigger root value
 * to the node with the smaller root value.
 * 
 * @param heap1 - to merge
 * @param heap2 - to mergeß
 * @return pointer to the merged node
 */
ExtraPriorityQueue::Node*
ExtraPriorityQueue::mergeNodes(Node* heap1, Node* heap2)
{
    if (heap1 == NULL)
        return heap2;
    if (heap2 == NULL)
        return heap1;
        
    if (heap1->value < heap2->value)
    {
        heap1->children.add(heap2);
        return heap1;
    }
    heap2->children.add(heap1);
    return heap2;
}

/**
 * @method: merge
 * usage: merge(minNode->children)
 * -------------------------------
 * Given a vector representation to any other Binomial Heap,
 * adds the given heap to this list.
 * 
 * for each heap in the vector, if its not null
 * try inserting from the index order
 *
 * @param other - vector of heaps to add to this list 
 */
void ExtraPriorityQueue::merge(Vector<Node*>& other)
{
    foreach(Node* current in other)
        if (current != NULL)
            insert(current->order(), current);
}

/**
 * @method: doubleLength
 * usage: if (index >= length) doubleLength();
 * -------------------------------
 * Creates a new array of Node*
 * and copies over the old heaps.
 * Fills the new half with NULL
 */
void ExtraPriorityQueue::doubleLength()
{
    Node** oldList = list;
    list = new Node*[length*2];
    
    for (int i=0; i<length; i++)
        list[i] = oldList[i];
    for (int i=length; i<length*2; i++)
        list[i] = NULL;
        
    length *= 2;
    delete[] oldList;
}

/**
 * @method: minIndex
 * usage: int index = minIndex()
 * --------------------------------
 * Returns the index of of heap node containing the smallest root value.
 * Starts with the first non-NULL heap node.
 * 
 * @exception isEmpty - cannot peek/dequeue from empty queue
 * @return min - index of the node containing the smallest root value
 */
int ExtraPriorityQueue::minIndex()
{
    if (isEmpty())
        error("No Such Element");
    
    int min = firstIndex();
	string minVal = list[min]->value;
    
    for (int i=min+1; i<length; i++)
        if (list[i] != NULL && list[i]->value < minVal)
        {
            min = i;
            minVal = list[i]->value;
        }
    return min;
}

/**
 * @method: firstIndex
 * usage: int min = firstIndex();
 * -------------------------------
 * Returns the index of the first non-NULL 
 * pointer to heap node in list.
 * 
 * @return i - index first non-NULL node pointer
 */
int ExtraPriorityQueue::firstIndex()
{
    for (int i=0; i<length; i++)
        if (list[i] != NULL)
            return i;
    return -1;
}

/**
 * @method: printList
 * usage: printList()
 * --------------------------
 * Prints the list to console.
 * for debugging purposes.
 */
void ExtraPriorityQueue::printList()
{
    string out = "[";
    for (int i=0; i<length; i++)
        if (list[i] != NULL)
            out += list[i]->value + ", ";
        else
            out += "NULL, ";
    out += "]";
    cout << out << endl;
}




