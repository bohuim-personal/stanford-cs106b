/**
 * File: pqueue-extra.h
 * ------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 */
#ifndef PQueue_Extra_Included
#define PQueue_Extra_Included

#include <cmath>
#include <iostream>
#include <string>
#include "vector.h"
using namespace std;
/**
 * Class: ExtraPriorityQueue
 * ------------------------------------
 * Implements a PriorityQueue backed by a Binomial Heap.
 * The indecies of a vector represents the (k)th degree binomial heap.
 * Enqueue merges current heap with order 1 heap.
 * Dequeue picks the smallest root value & merges its children with current heap.
 */
class ExtraPriorityQueue
{
    private:
        /** 
         * @struct: Node
         * ----------------------
         * Keeps track of value & its children Nodes
         */
        struct Node
        {
            /**
             * constructor: Node
             * ---------------------
             * sets value as given string
             */
            Node(string val)
            {
                value = val;
            }
            
            int order()
            {
                int exp = 0;
                exp += children.size();
                return pow(2.0, exp-1);
            }
            
            string value;
            Vector<Node*> children;
        };
        int length;
        Node** list;
        int numElements;

    public:
        /* Constructs a new, empty priority queue. */
        ExtraPriorityQueue();
        
        /* Cleans up all memory allocated by this priority queue. */
        ~ExtraPriorityQueue();
        
        /* Returns the number of elements in the priority queue. */
        int size();
        
        /* Returns whether or not the priority queue is empty. */
        bool isEmpty();
        
        /* Enqueues a new string into the priority queue. */
        void enqueue(string value);
        
        /* Returns, but does not remove, the lexicographically first string in the priority queue. */
        string peek();
        
        /* Returns and removes the lexicographically first string in the priority queue. */
        string dequeueMin();

        /* Given the vector of another Binomial Heap, merges it into this Binomial Heap's list */
        void merge(Vector<Node*>& other);
    
    private:    
        void insert(int index, Node* newHeap);
        Node* mergeNodes(Node* heap1, Node* heap2);
        void doubleLength();
        int minIndex();
        int firstIndex();
    
        void printList();
};

#endif
