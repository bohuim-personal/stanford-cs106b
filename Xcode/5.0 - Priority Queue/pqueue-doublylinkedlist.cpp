/**
 * File: pqueue-doublylinkedlist.cpp
 * --------------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 *  
 * Implements the member methods for the DoublyLinkedListPriorityQueue
 */
 
#include "pqueue-doublylinkedlist.h"
#include "error.h"

/**
 * Method: DoublyLinkedListPriorityQueue
 * Usage: DoublyLinkedListPriorityQueue *dpq = new DoublyLinkedListPriorityQueue();
 * ---------------------------------------------------------------------------------
 * Creates a new DoublyLinkedListPriorityQueue.
 * 
 * Sets first & last to NULL, numElements to 0
 */
DoublyLinkedListPriorityQueue::DoublyLinkedListPriorityQueue() 
{
	first = NULL;
    last = NULL;
    numElements = 0;
}

/**
 * Method: ~DoublyLinkedListPriorityQueue
 * Usage: delete dpq
 * -----------------------------
 * Traverses through the Linked List 
 * to delete all the nodes
 */
DoublyLinkedListPriorityQueue::~DoublyLinkedListPriorityQueue() 
{
	Node* current = first;
    Node* temp;
    for (int i=0; i<size(); i++)
    {
        temp = current->next;
        delete current;
        current = temp;
    }
}

/**
 * Method: size
 * Usage: dpq->size()
 * -----------------------------
 * Returns the number of elements
 * 
 * @return numElements - number of currently enqueued elements
 */
int DoublyLinkedListPriorityQueue::size() 
{
	return numElements;
}

/**
 * Method: isEmpty
 * Usage: dpq->isEmpty()
 * -----------------------------
 * Returns whether queue is empty
 * 
 * @return true if size is 0
 */
bool DoublyLinkedListPriorityQueue::isEmpty() 
{
	return size() == 0;
}

/**
 * Method: enqueue
 * Usage: dpq->enqueue("element")
 * -------------------------------
 * Adds the given string value to the queue.
 * 
 * Makes a new Node with the given value.
 * Sets new node's prev as last, and last as new node.
 * if queue is empty
 *      - also sets new node as first
 * 
 * increments numElements
 */
void DoublyLinkedListPriorityQueue::enqueue(string value) 
{
	Node* newNode = new Node(value);
    
    Node* oldLast = last;
    newNode->prev = oldLast;
    last = newNode;
    
    if (isEmpty())
        first = newNode;
    else
        oldLast->next = newNode;
        
    numElements++;
}

/**
 * Method: peek
 * Usage: dpq->peek()
 * -----------------------------
 * Returns but does not remove the smallest value.
 * 
 * @exception isEmpty - cannot peek at an empty queue
 * @return smallest element in the queue
 */
string DoublyLinkedListPriorityQueue::peek() 
{
    if (isEmpty())
        error("No Such Element");

	return getMinNode()->value;
}

/**
 * Method: dequeueMin
 * Usage: dpq->dequeueMin()
 * -----------------------------
 * Returns and removes the smallest value.
 * 
 * Gets the node containing the smallest value.
 * Stores the prev & next of the node, then deletes the node.
 * If either are NULL
 *      - set first/last accordingly
 * Decrement numElements
 * 
 * @exception isEmpty - cannot peek at an empty queue
 * @return smallest element in the queue
 */
string DoublyLinkedListPriorityQueue::dequeueMin() 
{
    if (isEmpty())
        error("No Such Element");

    Node* minNode = getMinNode();
    string min = minNode->value;
    
    Node* prev = minNode->prev;
    Node* next = minNode->next;
    delete minNode;
        
    if (next == NULL)
        last = prev;
    else
        next->prev = prev;
        
    if (prev == NULL)
        first = next;
    else
        prev->next = next;
    
    numElements--;
	return min;
}

/**
 * Method: getMinNode
 * Usage: Node* minNode = getMinNode()
 * -----------------------------------
 * Traverses the Linked List to find the
 * node containing the smallest element
 */
DoublyLinkedListPriorityQueue::Node* 
DoublyLinkedListPriorityQueue::getMinNode()
{
    Node* minNode = first;
    string min = first->value;
    
    for (Node* current=first; current != NULL; current=current->next)
        if (current->value < min)
        {
            min = current->value;
            minNode = current;
        }
    return minNode;
}
