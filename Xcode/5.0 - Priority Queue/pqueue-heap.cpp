/**
 * File: pqueue-heap.cpp
 * --------------------------------------
 * Author: Bohui Moon
 * Section: Steve Cooper
 * 
 * Implements the member methods for the HeapPriorityQueue
 */
 
#include "pqueue-heap.h"
#include "error.h"

/**
 * Constructor: HeapPriorityQueue
 * Usage: HeapPriorityQueue *hpq = new HeapPriorityQueue();
 * -----------------------------
 * Creates a new HeapPriorityQueue
 * 
 * Initially creates array with length 2 and sets cell 0 as empty string.
 * Sets numElements to 0.
 */
HeapPriorityQueue::HeapPriorityQueue() 
{
    length = 2;
    heap = new string[length];
    heap[0] = "";
    numElements = 0;
}

/**
 * Deconstruct: ~HeapPriorityQueue
 * Usage: delete hpq
 * -----------------------------
 * De-allocates the current array heap
 */
HeapPriorityQueue::~HeapPriorityQueue() 
{
	delete[] heap;
}

/**
 * Method: size
 * Usage: hpq->size()
 * -----------------------------
 * Returns size of the queue.
 * 
 * @return numElement - number of elements in the queue
 */
int HeapPriorityQueue::size() 
{
	return numElements;
}

/**
 * Method: isEmpty
 * Usage: hpq->isEmpty()
 * ----------------------------------
 * Returns whether the queue is empty
 * 
 * @return true if (size() == 0)
 */
bool HeapPriorityQueue::isEmpty() 
{
	return size() == 0;
}

/**
 * Method: enqueue 
 * Usage: hpq->enqueue("element")
 * -----------------------------
 * Adds the given string element to the queue.
 * 
 * Increments numElements first to add to end of array, accounting for empty first cell.
 * Before adding, if size is 0
 *      - double the length of the dynamic array
 * 
 * Add the given value to index of size() and bubble the element up
 *
 * @param value - string element to enqueue
 */
void HeapPriorityQueue::enqueue(string value) 
{
    numElements++;
    if (size() == length)
        doubleLength();
	heap[size()] = value;
    bubbleUp(size());
}

/**
 * Method: peek
 * Usage: hpq->peek()
 * -----------------------------
 * Returns but does not remove the first value.
 * 
 * @exception isEmpty - cannot peek at an empty queue
 * @return heap[1] (to account for empty first cell)
 */
string HeapPriorityQueue::peek() 
{
    if (isEmpty())
        error("No Such Element");
	return heap[1];
}

/**
 * Method: dequeueMin 
 * Usage: hpq->dequeueMin()
 * -----------------------------
 * Returns and removes the first element in the queue.
 * 
 * Stores heap[1] and swap it with the last element in the heap.
 * Set previous last position to empty string.
 * Decrements the numElements.
 * BubbleDown the newly swapped first element (that used to be the last)
 * 
 * @exception isEmpty - cannot dequeue from empty queue
 * @return val - stored value of the old first element
 */
string HeapPriorityQueue::dequeueMin() 
{
    if (isEmpty()) 
        error("No Such Element");

	string val = heap[1];
    heap[1] = heap[size()];
    heap[size()] = "";  
    numElements--;
    
    bubbleDown(1);
	return val;
}

/**
 * Method: doubleLength
 * Usage: doubleLength()
 * -----------------------------
 * Doubles the length of the underlying dynamic array.
 * 
 * Make the current heap the oldHeap and create a new heap.
 * Copy all the elements over, double length instance variable.
 * delete the oldHeap
 */
void HeapPriorityQueue::doubleLength()
{
    string* oldHeap = heap;
    heap = new string[length*2];
    
    for (int i=0; i<length; i++)
        heap[i] = oldHeap[i];
    
    length *= 2;
    delete[] oldHeap;
}

/**
 * Method: bubbleUp 
 * Usage: bubbleUp(index)
 * -----------------------------
 * Bubbles up the newly enqueued element.
 * 
 * Find the index of the parent element & store the parent element.
 * If the parent element is smaller than the element at the given index
 *      - swap the elements
 * Recursively bubble up the same element (now at parentIndex)
 * 
 * @param index - of element to bubble up
 */
void HeapPriorityQueue::bubbleUp(int index)
{
    int parentIndex = index/2;
    string target = heap[index];
    string parent = heap[parentIndex];
    
    if (parentIndex!=0 && target < parent)
    {
        heap[index] = parent;
        heap[parentIndex] = target;
        bubbleUp(parentIndex);
    }
}

/**
 * Method: bubbleDown
 * Usage: bubbleDown(1)
 * -----------------------------
 * Bubbles down the last element swapped into first place
 * 
 * Gets the childIndex by calling smallerChildIndex()
 * if the returned childIndex is not -1
 *      - get the child element
 *      - swap the target & child
 * Recursively bubble down the same element (now at childIndex)
 * 
 * @param index - of element to bubble down
 */
void HeapPriorityQueue::bubbleDown(int index)
{
    int childIndex = smallerChildIndex(index);
    string target = heap[index];
    
    if (childIndex > 0)
    {
        string child = heap[childIndex];
        heap[childIndex] = target;
        heap[index] = child;
        bubbleDown(childIndex);
    }
}

/**
 * Method: smallerChildIndex 
 * Usage: int childIndex = smallerChildIndex(index)
 * ------------------------------------------------
 * Finds the index of the appropriate child element to swap.
 * 
 * Get the indecies of the both left & right childs.
 * If the they are within size()
 *      - get the child at the indecies
 *      - otherwise set them as empty strings
 * 
 * if right child is not empty string & it's smaller than left child & smaller than target element
 *      - return right child
 * if left child is not empty string & its smaller than target element
 *      - return left index
 * otherwise return -1
 */
int HeapPriorityQueue::smallerChildIndex(int index)
{
    int leftIndex = index*2;
    int rightIndex = index*2 + 1;
    
    string leftChild = "";
    if (leftIndex <= size())
        leftChild = heap[leftIndex];

    string rightChild = "";
    if (rightIndex <= size())
        rightChild = heap[rightIndex];
        
    if (rightChild != "" && rightChild < leftChild && heap[index] > rightChild)
        return rightIndex;
    if (leftChild != "" && heap[index] > leftChild)
        return leftIndex;
    return -1;
}