/*
 * File: RandomWriter.cpp
 * ----------------------
 * Name: Bohui Moon
 * Section: Steve Cooper, Gates 100
 * This file is the starter project for the Random Writer problem.
 * 
 * Implements a Random Text Generator 
 * Using a input source text file, and a specified level of generated text (Markov order),
 * program makes a text of max 2000 letters (including spaces & punctuation) 
 * First runs through entire source file and stores what letter comes after a certain series of letters.
 * Based upon that frequency and probabiltiy, a random text is generated
 * 
 * Function:
 *      randomText() - master function calls all necessary inner functions for the program
 *      getNewSettings() - gets the source file and Markov order for every reading
 *      scanFile() - goes through source file letter by letter & records frequencies
 *      updateSeeds(ch) - given a letter, updates the queue of seeds & calls enterSeed() when appropriate
 *      enterSeed(string) - given a seed (string), adds the value to seedMap
 *      mostCommonSeed() - returns the most commonly occuring seed in the source file (used as initial seed)
 *      generateText(string) - given an initial seed, returns the randomly generated text 
 */

#include <iostream>
#include <fstream>
#include "map.h"
#include "vector.h"
#include "queue.h"
#include "strlib.h"
#include "foreach.h"
#include "random.h"
#include "console.h"
using namespace std;

/* 
 * Global Variables 
 */
ifstream infile;
int order;
Map< string, Vector<string> > seedMap;
Queue<string> currentSeeds;


/* 
 * Function Prototypes 
 */
string getLine(string mssg);
void randomText();
void getNewSettings();
void scanFile();
void updateSeeds(char &ch);
void enterSeed(string seed);
string mostCommonSeed();
string generateText(string initialSeed);
void debugMap();


/* Code */

/**
 * Function: main
 * Usage: int main()
 * -----------------------------------------------
 * calls randomText() to start program
 */
int main() 
{
    randomText();
    return 0;
}

/**
 * Function: randomText
 * Usage: randomText()
 * -----------------------------------------------
 * Indefinitely runs the random text generator by calling
 *      getNewSettings(), scanFile(), generateText(), mostCommonSeed()
 * Prints out the text returned by generateText()
 */
void randomText()
{
    while (true)
    {
        getNewSettings();
        scanFile();
        cout << generateText(mostCommonSeed()) << endl << endl;
    }
}

/**
 * Function: getNewSettings
 * Usage: getNewSettings()
 * -----------------------------------------------
 * Gets the necessary information for generating random text
 * Asks user for the source text file & Markov order to read
 * Repeatedly asked the user for both of these values until valid inputs are given
 */
void getNewSettings()
{
    //open the file in ifstream, run until a valid file is given
    bool fileOpened = false;
    while (!fileOpened) 
    {
        infile.close();
        infile.open(getLine("Input file name: ").c_str());
        if (infile.good())
            fileOpened = true;
        else
            cout << "Unable to open that file. Try again." << endl;
    }
    
    //get a valid Markov order
    fileOpened = false;
    while (!fileOpened)
    {
        order = stringToInteger(getLine("Enter the Markov order [1-10]: "));
        if (1<=order && order<=10)
            fileOpened = true;
        else
            cout << "That value is out of range." << endl;
    }
}

/**
 * Function: scanFile
 * Usage: scanFile()
 * -----------------------------------------------
 * Goes through the entire source file character by character 
 * Enters the seeds & the character occuring after by calling updateSeeds()
 * Calls enterSeed() one last time to account for the seed left in the Queue
 * 
 * Precondition: called after getNewSettings()
 */
void scanFile()
{
    currentSeeds.clear(); //reset any previous entries
    cout << "Processing file... " << endl << endl;
    
    char ch;
    while (infile.get(ch))
        updateSeeds(ch);
    
    //account for last seed that did not get proceeded due to length
    enterSeed(currentSeeds.dequeue());
}

/**
 * Function: updateSeeds
 * Usage: updateSeeds(&ch)
 * -----------------------------------------------
 * Goes through all seeds currently in the Queue
 *      if seed is of length order+1, send it off by calling enterSeed()
 *      otherwise add the given char to each seed and re-enqueue
 * Create a new seed with only the given char to enqueue
 * 
 * Param: &ch - next character in the soucre file
 */
void updateSeeds(char &ch)
{
    int run = currentSeeds.size();
    for (int i=0; i<run; i++)
    {
        string scanned = currentSeeds.dequeue();
        if (scanned.length() == order+1)
            enterSeed(scanned);
        else
        {
            scanned += ch;
            currentSeeds.enqueue(scanned);
        }
    }
    string newStr = "";
    newStr += ch;
    currentSeeds.enqueue(newStr);
}

/**
 * Function: enterSeed
 * Usage: enterSeed(scanned)
 * -----------------------------------------------
 * Given string is split into the seed & the char following 
 * Creates a new key in seedMap if seed was not already entered
 * add the char following the seed to the Vector connected through seedMap to record the frequency
 * 
 * Precondition: scanned string's length == order+1
 * Param: scanned - string to be separated into seed & char following seed
 */
void enterSeed(string scanned)
{
    string seed = scanned.substr(0,order);
    string next = scanned.substr(order,1);
    
    if (!seedMap.containsKey(seed))
        seedMap.put(seed, *new Vector<string>);
    seedMap[seed].add(next);
}

/**
 * Function: mostCommonSeed
 * Usage: generateText(mostCommonSeed())
 * -----------------------------------------------
 * Creates a Vector in case there are two or more seeds the appear the most.
 * Goes through each key in seedMap and compares the size of Vectors connected to the key.
 * More values in the Vector signifies more appearance of the key in the file
 * 
 * If size of current Vector > previous max 
 *      - reset max to size, clear the Vector seeds & add the current key to seeds
 * If size of current Vector = previous max
 *      - add the current key to seeds
 * Otherwise
 *      - do nothing
 *
 * Randomly select and return an element that occured the most from seeds
 * 
 * Return: string sequence that occurs the most often in the source text file
 */
string mostCommonSeed()
{
    Vector<string> seeds;
    seeds.add("");
    int max = 0;
    
    foreach (string key in seedMap)
    {
        int size = seedMap[key].size();
        if (size == max)
            seeds.add(key);
        else if (size > max)
        {
            seeds.clear();
            max = size;
            seeds.add(key);
        }
    }
    
    return seeds[randomInteger(0, seeds.size()-1)];
}

/**
 * Function: generateText
 * Usage: generateText(initialSeed)
 * -----------------------------------------------
 * Start with the given seed & index of 0
 * For 2000 characters or less: use the substring of generated text from index to index+order as seed
 * to get a random character to append to the text
 * Increment the index everytime a char is added 
 *
 * Special Case: Vector connected to the map has no entries
 *      - break from the loop and just return whats available
 * 
 * Param: initialSeed - the string sequence that appeared the most in the source file
 * Return: randomly generated text using the source file as a model
 */
string generateText(string initialSeed)
{
    string text = initialSeed;
    int index = 0;
    for (int i=order; i<=2000; i++) 
    {
        string seed = text.substr(index,order);
        int size = seedMap[seed].size();
        if (size == 0) //break if no more values are available
            return text;
        string next = seedMap[seed].get(randomInteger(0, size-1));
        text += next;
        index++;
    }
    
    return txt;
}