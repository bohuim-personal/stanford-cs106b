/*
 * File: WordLadder.cpp
 * ----------------------
 * Name: Bohui Moon
 * Section: Steve Cooper, Gates 100
 * This file is the starter project for the Word Ladder problem.
 * 
 * Implements a simple game called Word Ladder, in which a sequence of words is created 
 * from the starting to end word by changing one letter at a time.
 * Uses a Lexicon to store unchanging string, Queue for a series of ladders, and a Stack for a ladder.
 * 
 * Functions:
 *      shortestLadder(start, end) - returns the string of the shortest ladder found or "No ladder found"
 *      copyLadder(ladder) - returns a pointer to the copy of the given ladder
 */

#include <iostream>
#include <string>
#include "queue.h"
#include "lexicon.h"
#include "stack.h"
#include "foreach.h"
#include "console.h"
using namespace std;

/* 
 * Global Variables 
 */
const string alphabet = "abcdefghijklmnopqrstuvwxyz";
Queue< Stack<string>* > ladderQ;
Lexicon usedWords;
Lexicon lex("EnglishWords.dat");

/* 
 * Function Prototypes 
 */
string getLine(string mssg);
void wordLadder();
string shortestLadder(string &start, string &end);
Stack<string>* copyLadder(Stack<string> original);

/* Code */

/**
 * Function: main
 * Usage: int main()
 * ----------------------------------
 * Calls wordLadder() for program to run
 */
int main() 
{
    wordLadder();
    return 0;
}

/**
 * Function: wordLadder()
 * Usage: wordLadder()
 * ----------------------------------
 * Indefinitely runs asks user for start & end words 
 * prints out whatever is returned by shortestLadder
 */
void wordLadder()
{
    while (true)
    {
        string start = getLine("Enter start word: ");
        string end = getLine("Enter destination word: ");
        
        cout << shortestLadder(start, end) << endl << endl;
    }
}

/**
 * Function: shortestLadder
 * Usage: cout << shortestLadder(start, end) << endl;
 * ----------------------------------
 * Setup:
 *      - create a Stack<string> currentLadder to hold the ladder currenlty being dealt with
 *      - clear usedWords from last run
 *      - add starting word to usedWords & currenLadder
 *      - add reference of ladder to Queue of ladders being processed
 *
 * Loop: runs until no ladders left to process
 *      - dequeue a ladder and set it as the currentLadder
 *      - peek at that ladder and set it as the currenWord
 *      - check: if currentWord is the ending word
 *          - return the ladder in string form
 *      - Loop: otherwise go through each letter in the word & replace with each letter in the alphabet
 *          - check: if created word exists & that it has not been used yet
 *              - add the new word into usedWords
 *              - copy the current ladder, add the new word, and add the ladder to Queue
 * 
 * Param: &start - reference to starting word of the ladder
 * Param: &end   - reference to ending word of the ladder
 * Return: string representation of the ladder or "No ladder found" if no ladder exists
 */
string shortestLadder(string &start, string &end)
{
    Stack<string> currentLadder;

    usedWords.clear();
    usedWords.add(start);
    currentLadder.push(start);
    ladderQ.enqueue(&currentLadder);
    
    while (!ladderQ.isEmpty())
    {
        currentLadder = *ladderQ.dequeue();
        string currentWord = currentLadder.peek();
        
        if (currentLadder.peek() == end)
            return currentLadder.toString();
        
        for (int i=0; i<currentWord.length(); i++)
            for (int n=0; n<alphabet.length(); n++)
            {
                string newWord = currentWord;
                newWord[i] = alphabet[n];
                
                if (lex.contains(newWord) && !usedWords.contains(newWord))
                {
                    usedWords.add(newWord);
                    Stack<string>* copy = copyLadder(currentLadder);
                    copy->push(newWord);
                    ladderQ.enqueue(copy);
                }
            }
    }
    
    return "No ladder found";
}

/**
 * Function: copyLadder
 * Usage: Stack<string>* copy = copyLadder(currentLadder);
 * ----------------------------------
 * Because a ladder is implemented using a Stack, 
 * all elements have to popped & transferred into another stack to get the original order 
 * then popped again to into the final copy
 * Pointer to the final copy is returned
 * 
 * Param: original - ladder to copy
 * Return: Stack<string>* - pointer to the copied ladder
 */
Stack<string>* copyLadder(Stack<string> original)
{
    int run = original.size();
    Stack<string>* copy = new Stack<string>;
    Stack<string>* final= new Stack<string>;
    for (int i=0; i<run; i++)
        copy->push(original.pop());
    for (int i=0; i<run; i++)
        final->push(copy->pop());
    
    return final;
}