/**
 * File: Subsequences.cpp
 * ----------------------
 * Name: Bohui (Dennis) Moon
 * Section: Steve Cooper
 * This file is the starter project for the Subsequences problem on Assignment #3.
 * 
 * Provides the function to check whether a certain word is a subsequence of another
 *
 * Functions:
 *      - checkSubsequnce(): indefinitely gets user's input and passes it to isSubsequence()
 *      - isSubsequence(word, subseq): given a subseq, checks whether it is a subsequence of word
 * 
 * Cited:
 *      - Stack Overflow on printing boolean as text using boolalpha (http://stackoverflow.com/questions/12998764/printing-boolean-in-c);
 */

#include <iostream>
#include <string>
#include "console.h"
using namespace std;

/* Function Protoyptes */
    string getLine(string mssg);
    void checkSubsequence();
    bool isSubsequence(string text, string subsequence);

/**
 * Function: main
 * Usage: int main()
 * -------------------------------
 * Calls checkSubsequence() for the user to enter words
 */
int main() 
{
    checkSubsequence();
    return 0;
}

/**
 * Function: checkSubsequence
 * Usage: checkSubsequence()
 * ---------------------------------------
 * Indefinitely asks the user for two words
 * and prints out whether the latter is a subsequence of the former 
 * by calling isSubsequence()
 */
void checkSubsequence()
{
    while (true)
    {
        cout << boolalpha << isSubsequence(getLine("Input word: "), getLine("Input subsequence: "))
        << endl << endl;    //formatting
    }
}

/**
 * Function: isSubsequence
 * Usage: isSubsequence(word, subseq)
 * -------------------------------------------------
 * Given two strings, returns whether the second string is a subsequence of the first string.
 *
 * Base Case: subseq is emptry string
 *      - true: by definitiion empty strings are subsequences of all strings
 * Base Case: word is shorter than subseq
 *      - false: a longer word cannot be a subsequence of a shorter one
 *
 * Reduction:
 *      - if the first letters of both strings match -> progress both strings by cutting off the first letter
 *      - otherwise, progress only the first string
 */
bool isSubsequence(string text, string subseq)
{
    if (subseq == "")
        return true;
    if (text.length() < subseq.length())
        return false;
    
    if (text.substr(0,1) == subseq.substr(0,1))
        return isSubsequence(text.substr(1), subseq.substr(1));
    return isSubsequence(text.substr(1), subseq);
}