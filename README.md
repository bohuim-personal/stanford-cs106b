# Stanford CS 106B: Programming Abstractions in C++

Taken during Summer of 2013  
[Course Website](http://web.stanford.edu/class/archive/cs/cs106b/cs106b.1136/)

**Layout**

Assignments were in C++ through VS or xcode.  
Since I own a mac, all assignments are under the `/Xcode` directory.